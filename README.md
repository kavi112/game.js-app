Quiz app
=========


Prerequisites
-------------

Make sure you have brew installed on your mac. try to run Install homebrew on your mac:

`brew info`

If not. install it. follow the instructions on the bottom of this page: http://mxcl.github.io/homebrew/ 

Then install mongodb and nodejs:

`brew install mongodb`

`brew install nodejs`


Start the database server
-------------------------

Open a new terminal window, go to the lingoquiz folder

If it's the first run, you need to create a database folder:

`mkdir db`

After that, run this command to start the server:

`mongod --dbpath=db/`

Then verify it's running by checking that it responds on this url: http://localhost:28017/


Start the app server
--------------------

Open a new terminal window, go to the server folder inside your lingoquiz folder and install dependencies by running:

`npm install restify`

`npm install mongoose`

`npm install express`

Finally, start the appserver by running:

`node server.js`

Verify that the appserver is running by opening this url and check that it responds: http://localhost:8080/


Run the app
-----------

If it's the first time, open a new terminal window, go to the lingoquiz folder and run:

`./runlingoquiz.sh`

That will link the app folder inside the lingoquiz folder to a lingoquiz folder that's picked up by Spotify. From here on you can just open the app from inside spotify by going to `spotify:app:lingoquiz` or by running this command in the terminal:

`open spotify:app:lingoquiz`


Good luck.
