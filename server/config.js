(function(exports) {

	// ugly production detection
	exports.production = (process.cwd().indexOf('www') != -1);

	exports.defaultPassword = '!$132987312798312897123789312132';

	if (exports.production) {

		exports.environment = 'prod';
		exports.mongoUrl = 'mongodb://91.123.198.230/lingoquiz';
		exports.facebookAppId = '564395903578953';
		exports.facebookSecret = '840b98802f684eef755dbdfe5420ee8b';
		exports.facebookRedirect = 'http://lingo-api.possan.se/user/auth/facebook';

	} else {

		exports.environment = 'dev';
		exports.mongoUrl = 'mongodb://localhost/lingoquiz';
		exports.facebookAppId = '208752252617833';
		exports.facebookSecret = '332145532b471f90ef80411930f8dc1b';
		exports.facebookRedirect = 'http://localhost:8080/user/auth/facebook';

	}

})(exports);