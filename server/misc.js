(function(exports) {

	exports.setup = function(server, db) {

		server.get('/', function (req, res, next) {
			res.send('Nothing to see here.');
			return next();
		});

	}

})(exports);
