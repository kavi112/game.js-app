(function(scope) {

	var mongoose = require('mongoose');

	var userSchema = mongoose.Schema({
	    name: String,
	    username: String,
	    password: String,
	    avatar: String,
	    facebookId: String
	});

	var User = mongoose.model('User', userSchema);

	var gameSchema = mongoose.Schema({
		id: String,
		meta: Object
	});

	var Game = mongoose.model('Game', gameSchema);

	var authSchema = mongoose.Schema({
		username: String,
		userId: String,
		meta: Object
	});

	var Auth = mongoose.model('Auth', authSchema);

	scope.User = User;
	scope.Game = Game;
	scope.Auth = Auth;

})(exports);