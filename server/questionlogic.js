(function(exports) {

	var fs = require('fs');

	function shuffle(pool) {
		pool = pool || [];
		pool.sort(function(a, b) {
			return -1 + Math.random() * 2;
		});
		return pool;
	}

	function shuffle_and_cut(pool, max) {
		shuffle(pool);
		if (pool.length > max)
			pool.splice(max, pool.length - max);
		return pool;
	}

	var supportedLanguages = ['english', 'spanish', 'french'];

	var localized_languages = {
		'english': {
			'spanish': 'Spanish',
			'english': 'English',
			'french': 'French',
		},
		'spanish': {
			'spanish': 'español',
			'english': 'Inglés',
			'french': 'francés'
		},
		'french': {
			'spanish': 'espagnol',
			'english': 'Anglais',
			'french': 'français'
		}
	};

	var localized_questions = {
		'english': [
			'How would you say \"%1\" in %2?',
			'What\'s the %2 translation for \"%1\"?',
		],
		"french": [
			'Quel est le mot de \"%1\" pour %2?'
		],
		"spanish": [
			'¿Cómo se dice \"%1\" en %2?',
			'¿Cuál es la palabra \"%1\" por %2?'
		]
	}

	var Questions = function() {
		this.dataset = {};
	}

	Questions.prototype.groupPoolsIntoQuestions = function(pool, language, applanguage) {
		// console.log('groupPoolsIntoQuestions', pool);
		var queue = pool.map(function(x) { return x; });
		var pool2 = [];
		for(var i=0; i<Math.ceil(queue.length); i++) {

			var q1 = JSON.parse(JSON.stringify(queue.splice(0, 1)[0]));
			q1.correct = true;
			// console.log('q1', q1);

			// How do you say q1 in
			var phrase = q1.phrase;
			// var loc_id = this.dataset.phrase_by_language[applanguage][phrase];
			// console.log('loc_id', loc_id, applanguage, phrase);
			// var loc_phrase = dataset.localized_phrase_by_phrase[phrase][applanguage];
			// var loc_phrase = this.dataset.localized_phrase_by_id[loc_id];
			var loc_phrase = q1.localized;

			var alternatives = [];
			pool.forEach(function(x) { if (x.id != q1.id) alternatives.push(x); });
			shuffle_and_cut(alternatives, 3);

			// var q2 = queue.splice(0, 1)[0];
			// var q3 = queue.splice(0, 1)[0];
			// var q4 = queue.splice(0, 1)[0];
			var q2 = JSON.parse(JSON.stringify(alternatives[0]));
			var q3 = JSON.parse(JSON.stringify(alternatives[1]));
			var q4 = JSON.parse(JSON.stringify(alternatives[2]));

			q2.correct = false;
			q3.correct = false;
			q4.correct = false;

			// console.log('q2', q2);
			// console.log('q3', q3);
			// console.log('q4', q4);

			var alts = [q1, q2, q3, q4];
			shuffle(alts);

			// console.log('alts', alts);

			var localized_language =
				localized_languages[applanguage][language]
				||
				language;

			var q = localized_questions[applanguage];
			shuffle(q);
			if (q && q.length > 0) {
				q = q[0];
				q = q.replace('%1', phrase);
				q = q.replace('%2', localized_language);

				var item = {
					type: 'alt',
					question: q,
					// # id: loc_id,
					phrase: phrase,
					alts: alts
				}

				// console.log('item', item);
				pool2.push(item);
			}
		}
		return pool2;
	}

	Questions.prototype.generateLanguageQuestions = function(opts) {
		// language_by_phrase
		// phrase_by_language
		// console.log('generateLanguageQuestions', opts);
		var lang = opts.language || '';
		var applanguage = opts.applanguage || 'english';
		var total = opts.total || 5;
		var group = opts.group || '';

		// pick total * 4 phrases at random
		var pool = [];
		var pbl = this.dataset.phrase_by_language[lang];
		// console.log('pbl', pbl);
		for (var phrase in pbl) {
			// console.log(group, phrase);
			var id = pbl[phrase];
			// console.log('id', id);
			//	if (this.dataset.phrase_by_group[group] && this.dataset.phrase_by_group[group].indexOf(phrase) != -1) {
			// console.log('phrase', phrase, id);
			var loc_id = this.dataset.localized_phrase_by_language[applanguage][lang][id];
			// console.log('loc_id', loc_id);
			pool.push({
				phrase: phrase,
				localized: loc_id,
				id: id
			});
		//	}
		}
		shuffle(pool);
		// console.log('pool', pool);
		// shuffle_and_cut(pool, total * 4);
		ret = this.groupPoolsIntoQuestions(pool, lang, applanguage);
		// console.log(JSON.stringify(ret, null, 1));
		// console.log();
		return ret;
	}

	Questions.prototype.generateQuestions = function(opts) {
		// console.log('generateQuestions', opts);
		var langs = opts.languages || [];
		var applanguage = opts.applanguage || 'english';
		var total = opts.total || 5;
		var groups = opts.groups || [];
		var _this = this;

		if (groups.length == 0) {
			langs.forEach(function(lang) {
				var tmp = _this.getGroupsForLanguage(lang);
				tmp.forEach(function(group) {
					groups.push(group.id);
				});
			});
		}

		// console.log('langs', JSON.stringify(langs));
		// console.log('groups', JSON.stringify(groups));

		// pick total * 4 * langs.length phrases at random
		var pool = [];
		langs.forEach(function(lang) {
			// console.log('get questions for language', lang);
			var bylang = _this.generateLanguageQuestions({
				language: lang,
				applanguage: applanguage,
				total: total,
				groups: groups
			});
			// console.log('bylang', bylang);
			pool = pool.concat(bylang);
		});
		// console.log(JSON.stringify(pool, null, 2));
		shuffle_and_cut(pool, total);
		// console.log(JSON.stringify(pool, null, 2));
		// console.log();

		return pool;
	}


	Questions.prototype.loadFile = function(filename) {
		var dataset = JSON.parse(fs.readFileSync(filename, 'UTF-8'));
		this.loadData(dataset);
	}

	Questions.prototype.loadData = function(dataset) {
		this.dataset = dataset;
	}

	Questions.prototype.getAllGroups = function() {
		var groups = [];
		for(var grp in this.dataset.language_by_group) {
			groups.push(grp);
		};
		return groups;
	}

	Questions.prototype.getGroupsForLanguage = function(language) {
		var groups = [];
		for(var grp in this.dataset.language_by_group) {
			if (this.dataset.language_by_group[grp].indexOf(language) != -1) {
				var grpobj = this.dataset.groups[grp];
				if (typeof(grpobj) !== 'undefined') {
					groups.push(grpobj);
				}
			};
		};
		return groups;
	}

	Questions.prototype.getLanguages = function() {
		var langs = [];
		for(var grp in this.dataset.language_by_group) {
			this.dataset.language_by_group[grp].forEach(function(lang) {
				if (langs.indexOf(lang) == -1)
					langs.push(lang);
			});
		};
		return langs;
	}

	Questions.prototype.getTrackInfo = function(id) {
		return {};
		/*
			spotifyUri: this.dataset.track_by_id[id],
			start: this.dataset.start_by_id[id],
			stop: this.dataset.stop_by_id[id]
		}; */
 	}

	Questions.prototype.dumpQuestions = function(title, questions) {
		var _this = this;
		console.log(title);
		console.log('---------------------------------------------------------------');
		questions.forEach(function(question) {
			console.log();
			console.log('  ' + question.question); // + ' ### ' + JSON.stringify(_this.getTrackInfo(question.id)));
			question.alts.forEach(function(alt) {
				console.log('   * '+alt.localized + ' #'+alt.id+' ' + ((alt.correct === true)?'(correct)':''));// + ' ### ' + JSON.stringify(_this.getTrackInfo(alt.id)));
			});
		});
		// console.log(JSON.stringify(questions));
		console.log();
	}

	var g_instance = null;

	Questions.getInstance = function() {
		if (g_instance == null) {
			g_instance = new Questions();
		}
		return g_instance;
	}

	exports.Questions = Questions;

})(exports);
