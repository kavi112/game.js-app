(function(exports) {

	var config = require('./config');
	var model = require('./model');

	var hardcoded_tokens = [
		{
			token: 'testtoken1',
			username: 'test1',
			password: 'test1',
			userid: 'testuser1',
		},
		{
			token: 'testtoken2',
			username: 'test2',
			password: 'test2',
			userid: 'testuser2',
		},
		{
			token: 'testtoken3',
			username: 'test3',
			password: 'test3',
			userid: 'testuser3',
		},
		{
			token: 'testtoken4',
			username: 'test4',
			password: 'test4',
			userid: 'testuser4',
		}
	];

	var Token = function(id) {
		this.id = id || '';
		this.userId = '';
		this.metadata = {};
		this.valid = false;
	}

	Token.load = function(token, callback) {
		console.log('Loading token #' + token);
		model.Auth.find({_id: token}, function(err, authobj) {
			if (err || authobj.length == 0) {
				callback(new Token(token));
			} else {
				var auth = authobj[0];
				console.log('got auth', auth);
				var tokenobj = new Token();
				tokenobj.id = auth.id;
				tokenobj.userId = auth.userId || '';
				tokenobj.metadata = auth.meta || {};
				// TODO: handle timeouts!!!
				tokenobj.valid = true;
				// tokenobj.id = auth.id;
				callback(tokenobj);
			}
		});
	}

	Token.forRequest = function(request, callback) {
		// console.log('Token for request', request);
		var t = request.params.access_token || '';
		Token.load(t, callback);
	}

	Token.prototype.save = function(callback) {
		// async update.
		var self = this;
		if (this.id == '') {
			// insert a new one!
			var auth = new model.Auth();
			auth.userId = self.userId;
			// auth.username = self.username;
			auth.meta = self.metadata;
			auth.updated = new Date();
			auth.save(function(err, data) {
				if (data) {
					self.valid = true;
					self.id = data._id;
				}
				if (callback)
					callback(self);
			});
		} else {
			model.Auth.find({_id: this.id}, function(err, authobj) {
				if (err) {
					console.log('Auth save error', err);
					// silent fail.
					callback(null);
				} else {
					var auth = authobj[0];
					auth.userId = self.userId;
					// auth.username = self.username;
					auth.meta = self.metadata;
					auth.updated = new Date();
					auth.save(function(err, data) {
						if (data) {
							self.valid = true;
							self.id = data._id;
						}
						if (callback)
							callback(self);
					});
				}
			});
		}
	}

	var AuthHelper = {};

	AuthHelper.loadToken = function(token, callback) {
		Token.load(token, callback);
	}

	AuthHelper.validateToken = function(token, callback) {
		Token.load(token, callback);
	}

	/*
	AuthHelper.renewToken = function(token, callback) {
		callback({
			valid: false,
			renewed: true,
			token: token
		});
	}
	*/

	AuthHelper.getUser = function(id, callback) {
		model.User.find({_id: id}, function(err, users) {
			if (err) {
				console.log('User find error', err);
				callback(null);
				return;
			}
			console.log('Got users', users);
			if (users.length == 0) {
				callback(null);
				return;
			}
			var user = users[0];
			callback(user);
		});
		model.User.find({}, function(err, users) {
			console.log('model User', err, users);
		});
	}

	AuthHelper.getOrCreateFacebookToken = function(facebookid, facebookname, facebookpicture, facebooktoken, callback) {
		model.User.find({facebookId: facebookid}, function(err, users) {
			if (err) {
				console.log('User find error', err);
				callback(new Token());
				return;
			}
			console.log('Got users', users);
			if (users.length == 0) {
				console.log('No such user, creating one..', err);
				// create a user
				var user = new model.User();
				user.facebookId = facebookid;
				user.name = facebookname;
				user.password = config.defaultPassword;
				user.avatar = facebookpicture;
				user.save(function(err, data) {
					console.log('saved user', user, err, data);
					// if (data) {
					AuthHelper.createToken(function(token) {
						console.log('creating new token', token);
						// token.username = user.username;
						token.userId = /*user.id ||*/ data._id;
						token.metadata.facebookToken = facebooktoken;
						token.metadata.facebookId = facebookid;
						token.save(function(token) {
							callback(token);
						});
					});
					// }
				})
				return;
			}
			var user = users[0];
			AuthHelper.createToken(function(token) {
				// token.username = user.username;
				token.userId = user._id;
				token.metadata.facebookToken = facebooktoken;
				token.metadata.facebookId = facebookid;
				token.save(function(token) {
					callback(token);
				});
			});
		});
	}

	AuthHelper.validateUsernameAndPassword = function(username, password, callback) {
		// load user
		model.User.find({username: username}, function(err, users) {
			if (err) {
				console.log('User load error', err);
				// silent fail.
				callback(new Token());
				return;
			}
			console.log('Got users', users);
			if (users.length == 0) {
				// no user.
				callback(new Token());
				return;
			}
			var user = users[0];
			if (user.password != password) {
				// wrong password.
				callback(new Token());
				return;
			}
			// AuthHelper.createToken(function(token) {
			// token.username = user.username;
			var token = new Token();
			token.userId = user.id;
			token.save(function(token) {
				callback(token);
			});
			// });
		});
	}

	AuthHelper.createToken = function(callback) {
		var token = new Token();
		token.save(function(token) {
			token.valid = true;
			callback(token);
		});
	}

	AuthHelper.forRequest = function(request, callback) {
		Token.forRequest(request, callback);
	}

	exports.AuthHelper = AuthHelper;

})(exports);