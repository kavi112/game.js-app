(function(exports) {

	exports.setup = function(server, db) {

		// http://localhost:8080/questions/generate?group=Greetings&language=Spanish&applanguage=English

		server.get('/questions/generate/?', function (req, res, next) {
			console.log('get questions', req.params, req.query);

			var groups = [];
			if (req.params.group) groups.push(req.params.group);
			var languages = req.params.languages || 'english';
			var applanguage = req.params.applanguage || 'english';
			var total = req.params.total || 3;

			var questions = server.q.generateQuestions({
				languages: languages.split(','),
				applanguage: applanguage,
				total: total,
				groups: groups
			});

			if (questions) questions.forEach(function(question) {
				console.log('question', question);
				// question.track = server.q.getTrackInfo(question.id);
				question.alts.forEach(function(alt) {
					console.log('alt', alt);
					// alt.track = server.q.getTrackInfo(alt.id);
				});
			});

			// console.log(questions);
			res.setHeader('Content-Type', 'application/json; charset=utf-8');
			res.charset = 'UTF-8';
			res.write(JSON.stringify(questions, 2, null));
			res.end()
			return next();
		});

		server.get('/languages/:lang/?', function (req, res, next) {
			console.log('get info about language', req.params, req.query);
			var ret = server.q.getGroupsForLanguage(req.params.lang);
			// res.send(ret);
			res.setHeader('Content-Type', 'application/json; charset=utf-8');
			res.charset = 'UTF-8';
			res.write(JSON.stringify(ret, 2, null));
			res.end()
			return next();
		});

		server.get('/questions/?', function (req, res, next) {
			console.log('get questions in group', req.params, req.query);
			var ret = {
				languages: server.q.getLanguages(),
				groups: server.q.getAllGroups(),
				groups_by_language: {}
			};
			server.q.getLanguages().forEach(function(lang) {
				ret.groups_by_language[lang] = server.q.getGroupsForLanguage(lang);
			});
			res.setHeader('Content-Type', 'application/json; charset=utf-8');
			res.charset = 'UTF-8';
			res.write(JSON.stringify(ret, 2, null));
			res.end()
			// res.send(ret);
			return next();
		});

		server.get('/languages/?', function (req, res, next) {
			console.log('get list of languages', req.params, req.query);
			var ret = server.q.getLanguages();
			res.setHeader('Content-Type', 'application/json; charset=utf-8');
			res.charset = 'UTF-8';
			res.write(JSON.stringify(ret, 2, null));
			res.end()
			// res.send(ret);
			return next();
		});
	}

})(exports);
