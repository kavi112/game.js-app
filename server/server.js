var restify = require('restify');

var questionlogic = require('./questionlogic')

var config = require('./config');
var model = require('./model');
var mongoose = require('mongoose');
var AuthHelper = require('./authhelper').AuthHelper;

var authmod = require('./auth');
var gamesmod = require('./games');
var lobbymod = require('./lobby');
var miscmod = require('./misc');
var questionsmod = require('./questions');

console.log('Environment: '+config.environment);
console.log('Connecting to db: ' + config.mongoUrl)
mongoose.connect(config.mongoUrl);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  // yay!
  console.log('db connected.');
});

// AuthHelper.setup(db);

var server = restify.createServer({
	name: 'myapp',
	version: '1.0.0',
	formatters: {
        'text/html': function(req, res, body) {
	        if (body instanceof Error) {
                res.statusCode = body.statusCode || 500;
                body = body.message;
	        } else if (typeof (body) === 'object') {
	                body = JSON.stringify(body);
	        } else {
	                body = body.toString();
	        }
	        res.setHeader('Content-Length', Buffer.byteLength(body));
	        return (body);
        }
    }
});


server.q = new questionlogic.Questions();
server.q.loadFile('../dataset/ripped.json');

console.log('Supported languages', server.q.getLanguages());
console.log('All question groups', server.q.getAllGroups());
server.q.getLanguages().forEach(function(lang) {
	console.log('Question groups for language', lang, server.q.getGroupsForLanguage(lang));
});


server.use(
  function crossOrigin(req,res,next) {
  	console.log('Adding cors headers');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, DELETE");
    return next();
  }
);

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.jsonp());

authmod.setup(server, db);
gamesmod.setup(server, db);
lobbymod.setup(server, db);
miscmod.setup(server, db);
questionsmod.setup(server, db);

/*
if (module && module.exports) {

	module.exports = server;

} else {
*/
server.listen(8080, function () {
	console.log('%s listening at %s', server.name, server.url);
});
// }
