(function(exports) {

	var restify = require('restify');

	var config = require('./config');
	var model = require('./model');
	var AuthHelper = require('./authhelper').AuthHelper;
	var Game = require('./gamelogic').Game;

	exports.setup = function(server, db) {

		server.post('/game', function (req, res, next) {

			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					console.log('create game');
					console.log(req.query);
					console.log(req.params);
					console.log('\n\n');
					res.contentType = 'text/plain';

					var g = new model.Game();
					var game = new Game();

					if (req.params.turns)
						game.totalTurns = parseInt(req.params.turns, 10);

					var create_players = (req.params.players || '').split(',');
					create_players = create_players.splice(0, 1);
					console.log('all opponents', create_players);

					console.log('token', token);

					AuthHelper.getUser(token.userId, function(me) {

						console.log('me', me);

						if (create_players[0] == '') {

							// practice run

							game.addPlayer(me._id, me.name, me.avatar);
							game.beginInvite();
							game.acceptInvite(me._id, req.params.mylanguage); // jag har ju accepterat redan såklart!
							// game.start();

							g.meta = {};
							game.saveTo(g.meta);

							g.save(function(err, data) {
								console.log('save callback', err, data);
								if (err) {
									// error
									res.send('');
									next();
								}
								else if (data) {
									res.contentType = 'application/json';
									res.send({
										id: g._id,
										meta: g.meta
									});
									next();
								}
							});

						} else {

							// actual opponent

							AuthHelper.getUser(create_players[0], function(opponent) {
								console.log('opponent', opponent);

								game.addPlayer(opponent._id, opponent.name, opponent.avatar);
								game.addPlayer(me._id, me.name, me.avatar);

								game.beginInvite();
								game.acceptInvite(me._id, req.params.mylanguage); // jag har ju accepterat redan såklart!

								g.meta = {};
								game.saveTo(g.meta);

								g.save(function(err, data) {
									console.log('save callback', err, data);
									if (err) {
										// error
										res.send('');
										next();
									}
									else if (data) {
										res.contentType = 'application/json';
										res.send({
											id: g._id,
											meta: g.meta
										});
										next();
									}
								});
							});
						}
					});
				}
			});
		});

		server.get('/game/:id', function (req, res, next) {
			// console.log('get game');
			// console.log(req.query);
			// console.log(req.params);
			// console.log('\n\n');
			model.Game.find({ _id: req.params.id }, function(err, game) {
				var gamemodel = game[0];
				var gameobj = new Game();
				gameobj.loadFrom(gamemodel.meta);
				// console.log(gameobj);
				res.contentType = 'application/json';
				res.send({
					id: gamemodel._id,
					meta: gamemodel.meta
				});
				next();
			});
			return null;
		});

		server.post('/game/:id/accept', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					model.Game.find({ _id: req.params.id }, function(err, games) {

						console.log('found game', err, games);

						res.contentType = 'text/plain';

						if (err) {
							res.send('');
							next();
							return;
						}

						if (games.length == 0) {
							res.send('');
							next();
							return;
						}

						var gamemodel = games[0];
						var gameobj = new Game();

						console.log('gamemodel.meta', gamemodel.meta);
						gameobj.loadFrom(gamemodel.meta);

						console.log('gameobj', gameobj);
						gameobj.acceptInvite(token.userId, req.params.mylanguage);

						console.log('gameobj after progress', gameobj);
						gameobj.saveTo(gamemodel.meta);

						console.log('gamemodel.meta', gamemodel.meta);
						model.Game.update({_id: req.params.id}, {meta: gamemodel.meta}, {upsert: true}, function(err, data) {

							console.log('save callback', err, data);

							if (err) {
								// error
								res.send('');
								next();
								return;
							}

							// if (data) {
							// inserted.
							res.contentType = 'application/json';
							res.send({
								id: gamemodel._id,
								meta: gamemodel.meta
							});
							next();
							// }
						});
					});
				}
			});
			return null;
		});


		server.post('/game/:id/decline', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					model.Game.find({ _id: req.params.id }, function(err, games) {

						console.log('found game', err, games);

						res.contentType = 'text/plain';

						if (err) {
							res.send('');
							next();
							return;
						}

						if (games.length == 0) {
							res.send('');
							next();
							return;
						}

						var gamemodel = games[0];
						var gameobj = new Game();

						console.log('gamemodel.meta', gamemodel.meta);
						gameobj.loadFrom(gamemodel.meta);

						console.log('gameobj', gameobj);
						gameobj.declineInvite(token.userId);

						console.log('gameobj after progress', gameobj);
						gameobj.saveTo(gamemodel.meta);

						console.log('gamemodel.meta', gamemodel.meta);
						model.Game.update({_id: req.params.id}, {meta: gamemodel.meta}, {upsert: true}, function(err, data) {

							console.log('save callback', err, data);

							if (err) {
								// error
								res.send('');
								next();
								return;
							}

							res.contentType = 'application/json';
							res.send({
								id: gamemodel._id,
								meta: gamemodel.meta
							});
							next();
						});
					});
				}
			});
			return null;
		});


		server.post('/game/:id/meta', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					var progress_meta = JSON.parse(req.params.meta || '{}');

					model.Game.find({ _id: req.params.id }, function(err, games) {

						console.log('found game', err, games);

						res.contentType = 'text/plain';

						if (err) {
							res.send('');
							next();
							return;
						}

						if (games.length == 0) {
							res.send('');
							next();
							return;
						}

						var gamemodel = games[0];
						var gameobj = new Game();

						console.log('gamemodel.meta', gamemodel.meta);
						gameobj.loadFrom(gamemodel.meta);

						console.log('gameobj', gameobj);
						gameobj.updateGameMeta(token.userId, progress_meta);

						if (req.params.popfirstquestion) {
							// pop first item from users' question queue
						}

						console.log('gameobj after progress', gameobj);
						gameobj.saveTo(gamemodel.meta);

						console.log('gamemodel.meta', gamemodel.meta);
						model.Game.update({_id: req.params.id}, {meta: gamemodel.meta}, {upsert: true}, function(err, data) {

							console.log('save callback', err, data);

							if (err) {
								// error
								res.send('');
								next();
								return;
							}

							res.contentType = 'application/json';
							res.send({
								id: gamemodel._id,
								meta: gamemodel.meta
							});
							next();
						});
					});
				}
			});
			return null;
		});

		server.post('/game/:id/questions', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					var progress_meta = JSON.parse(req.params.meta || '{}');
					var groups = [];
					if (req.params.group)
						groups.push(req.params.group);
					var languages = req.params.languages || 'english';
					var applanguage = req.params.applanguage || 'english';
					var total = req.params.total || 10;

					model.Game.find({ _id: req.params.id }, function(err, games) {

						console.log('found game', err, games);

						res.contentType = 'text/plain';

						if (err) {
							res.send('');
							next();
							return;
						}

						if (games.length == 0) {
							res.send('');
							next();
							return;
						}

						var gamemodel = games[0];
						var gameobj = new Game();

						console.log('gamemodel.meta', gamemodel.meta);
						gameobj.loadFrom(gamemodel.meta);

						var questions = [];
						if (req.params.questions) {
							console.log('get questions from post');
							questions = JSON.parse(req.params.questions);
						} else {
							console.log('generate questions');
							questions = server.q.generateQuestions({
								languages: languages.split(','),
								applanguage: applanguage,
								total: total,
								groups: groups
							});

							if (questions) questions.forEach(function(question) {
								console.log('question', question);
								question.track = server.q.getTrackInfo(question.id); // console.log('  ' + question.question+ ' ### ' + JSON.stringify(_this.getTrackInfo(question.id)));
								question.alts.forEach(function(alt) {
									console.log('alt', alt);
									alt.track = server.q.getTrackInfo(alt.id);
								});
							});
						}
						console.log('questions', questions);

						console.log('gameobj', gameobj);
						gameobj.updateGameMeta(token.userId, { questions: questions });

						// update all players queues.
						gameobj.players.forEach(function(p) {
							p.questionQueue = questions;
						});

						console.log('gameobj after progress', gameobj);
						gameobj.saveTo(gamemodel.meta);

						console.log('gamemodel.meta', gamemodel.meta);
						model.Game.update({_id: req.params.id}, {meta: gamemodel.meta}, {upsert: true}, function(err, data) {

							console.log('save callback', err, data);

							if (err) {
								// error
								res.send('');
								next();
								return;
							}

							res.contentType = 'application/json';
							res.send({
								id: gamemodel._id,
								meta: gamemodel.meta
							});
							next();
						});
					});
				}
			});
			return null;
		});

		server.post('/game/:id/progress', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					var progress_meta = JSON.parse(req.params.meta || '{}');

					var is_delta_update = req.params.delta || false;
					if (typeof(progress_meta._delta) !== 'undefined' && progress_meta._delta === true) {
						is_delta_update = true;
					}
					if (typeof(progress_meta._delta) !== 'undefined') {
						delete(progress_meta._delta);
					}

					model.Game.find({ _id: req.params.id }, function(err, games) {
						console.log('found game', err, games);

						res.contentType = 'text/plain';

						if (err) {
							res.send('');
							next();
							return;
						}

						if (games.length == 0) {
							res.send('');
							next();
							return;
						}

						var gamemodel = games[0];
						var gameobj = new Game();

						console.log('gamemodel.meta', gamemodel.meta);
						gameobj.loadFrom(gamemodel.meta);

						console.log('gameobj', gameobj, is_delta_update);
						gameobj.updateProgress(token.userId, progress_meta, is_delta_update);

						console.log('gameobj after progress', gameobj);
						gameobj.saveTo(gamemodel.meta);

						console.log('gamemodel.meta', gamemodel.meta);
						model.Game.update({_id: req.params.id}, {meta: gamemodel.meta}, {upsert: true}, function(err, data) {

							console.log('save callback', err, data);

							if (err) {
								// error
								res.send('');
								next();
								return;
							}

							res.contentType = 'application/json';
							res.send({
								id: gamemodel._id,
								meta: gamemodel.meta
							});
							next();
						});
					});
				}
			});
			return null;
		});

		server.get('/games', function (req, res, next) {
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				if (token) {
					console.log('list a users games!');
					model.Game.find(
						{
							"meta.players.id": {
								"$exists": token.userId
							}
						},
						function(err, games) {
							if (err) {
							}
							else if (games) {
								var ret = [];
								// console.log(games);
								games.forEach(function(game) {
									// double check if i'm a player in this game.

									var im_a_player = false;
									var opponent_name = '';
									game.meta.players.forEach(function(p) {
										if (p.id == token.userId) {
											im_a_player = true;
										} else {
											opponent_name = p.name;
										}
									});

									if (!im_a_player)
										return;

									var age = Math.round(new Date() - game.meta.lastUpdated) / (24*60*60);
									console.log(game._id, game.meta.state, game.meta.turn, age);

									if (age > 40)
										return;

									ret.push({
										id: game._id,
										currentPlayerId: game.meta.currentPlayerId,
										lastUpdated: game.meta.lastUpdated,
										age: age,
										turn: game.meta.turn,
										state: game.meta.state,
										players: game.meta.players,
										opponentName: opponent_name
									});
								});

								// sort by status first, date second.

								ret.sort(function(a, b) {
									var d0 = 0;

									var _a = (a.state == 'active') ? 1 : ((a.state == 'over') ? -1 : 0);
									var _b = (b.state == 'active') ? 1 : ((b.state == 'over') ? -1 : 0);
									d0 = _b - _a;
									if (d0 != 0)
										return d0;

									if (b.lastUpdated && a.lastUpdated) {
										d0 = Math.round(b.lastUpdated - a.lastUpdated);
										if (d0 != 0)
											return d0;
									}

									d0 = b.turn - a.turn;
									if (d0 != 0)
										return d0;

									return 0;
								});

								res.contentType = 'application/json';
								res.send(ret);
							}
							next();
						}
					);
				}
			});
			return null;
		});

		server.get('/request/:id', function (req, res, next) {
			var id = req.params.id;
			console.log('get game request ' + id);
		 	console.log(req.query);
			console.log(req.params);
			console.log('\n\n');

			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);

				var client = restify.createStringClient({
					url: 'https://graph.facebook.com'
				})
				var path = '/' + id + '?access_token=' + token.metadata.facebookToken;
				console.log('path', path);
				client.get(path, function(err2, req2, res2, data) {
					console.log('got data:', data);
					res.send(data);
					next();
				});

			});

			return null;
		});

		server.post('/request/:id/delete', function (req, res, next) {
			var id = req.params.id;
			console.log('delete game request ' + id);
		 	console.log(req.query);
			console.log(req.params);
			console.log('\n\n');

			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);

				var client = restify.createStringClient({
					url: 'https://graph.facebook.com'
				})
				var path = '/' + id + '?access_token=' + token.metadata.facebookToken;
				console.log('path', path);
				client.del(path, function(err2, req2, res2, data) {
					console.log('got data:', data);
					res.send(data);
					next();
				});

			});

			return null;
		});

	}

})(exports);

