(function(exports) {

	var Game = function(_state) {
		this.reset();
		if (_state) this.loadFrom(_state);
	}

	Game.prototype.reset = function() {
		this.players = [];
		this.turn = 0;
		this.state = 'new';
		this.totalTurns = 3;
		this.turns = [];
		this.currentPlayerIndex = -1;
		this.currentPlayerId = '';
		this.currentPlayerQuestions = {};
		this.meta = {};
		this.result = null;
		this.lastUpdated = new Date();
	}

	Game.prototype.loadFrom = function(state) {
		if (state) {
			if (state.players) this.players = state.players;
			if (state.turn) this.turn = state.turn;
			if (state.state) this.state = state.state;
			if (state.totalTurns) this.totalTurns = state.totalTurns;
			if (state.turns) this.turns = state.turns;
			if (state.result) this.result = state.result;
			if (typeof(state.currentPlayerIndex) !== 'undefined') this.currentPlayerIndex = state.currentPlayerIndex;
			if (state.currentPlayerId) this.currentPlayerId = state.currentPlayerId;
			if (state.currentPlayerQuestions) this.currentPlayerQuestions = state.currentPlayerQuestions;
			if (state.lastUpdated) this.lastUpdated = state.lastUpdated;
			if (state.meta) this.meta = state.meta;
		}
	}

	Game.prototype.saveTo = function(target) {
		if (target) {
			target.players = this.players;
			target.turn = this.turn;
			target.state = this.state;
			target.totalTurns = this.totalTurns;
			target.turns = this.turns;
			target.currentPlayerIndex = this.currentPlayerIndex;
			target.currentPlayerId = this.currentPlayerId;
			target.currentPlayerQuestions = this.currentPlayerQuestions;
			target.meta = this.meta;
			target.result = this.result;
			target.lastUpdated = this.lastUpdated;
		}
	}

	Game.prototype.addPlayer = function(player, name, avatar) {
		this.players.push({
			id: player,
			name: name || '',
			avatar: avatar || '',
			state: 'invited',
			score: 0,
			turn: 0,
			place: 0,
			token: ''
		});
		this.currentPlayerIndex = 0;
		this.currentPlayerQuestions = {};
		this.lastUpdated = new Date();
		// this.state = 'active';
		this._updatePlayerStates();
	}

	Game.prototype.myTurn = function(player) {
		if (this.state != 'active') return false;
		var cp = this.players[this.currentPlayerIndex];
		return (cp.id == player && cp.state == 'current');
	}

	Game.prototype._updatePlayerStates = function() {
		this.currentPlayerId = '';
		this.currentPlayerQuestions = {};
		if (this.state == 'inviting' || this.state == 'new' || this.state == 'cancelled')
			return;
		for (var i=0; i<this.players.length; i++) {
			if (this.turn > this.totalTurns-1) {
				// game over
				this.players[i].state = 'done';
			} else if (this.turn == this.totalTurns-1) {
				// last turn
				if (i < this.currentPlayerIndex)
					this.players[i].state = 'done';
				else if (i == this.currentPlayerIndex) {
					this.players[i].state = 'current';
					this.currentPlayerId = this.players[i].id;
					this.currentPlayerQuestions = this.players[i].questionQueue;
				} else
					this.players[i].state = 'pending';
			} else {
				// previous turns
				if (i == this.currentPlayerIndex) {
					this.players[i].state = 'current';
					this.currentPlayerId = this.players[i].id;
					this.currentPlayerQuestions = this.players[i].questionQueue;
				} else
					this.players[i].state = 'pending';
			}
		}
	}

	Game.prototype._calcResults = function() {
		var pord = [];
		for (var i=0; i<this.players.length; i++) {
			pord.push({
				player: this.players[i].id,
				score: this.players[i].score
			});
		}
		pord.sort(function(a, b) {
			return b.score - a.score;
		});
		var winner = '';
		if (pord.length > 1 && pord[0].score > pord[1].score) {
			winner = pord[0].player;
		}
		this.result = {
			winner: winner,
			draw: winner == '',
			players: pord,
		};
	}

	Game.prototype.acceptInvite = function(player, language) { this.setInviteStatus(player, true, language); }
	Game.prototype.declineInvite = function(player, language) { this.setInviteStatus(player, false, language); }

	Game.prototype.setInviteStatus = function(player, ok, language) {
		if (this.state != 'inviting')
			return;

		// set status for player
		for(var i=0; i<this.players.length; i++) {
			if (this.players[i].id == player) {
				this.players[i].state = ok ? 'accepted' : 'declined';
				this.players[i].language = language;
				this.lastUpdated = new Date();
			}
		}

		// if all are accepted, start game.
		var any_not_accepted = false;
		for(var i=0; i<this.players.length; i++) {
			if (this.players[i].state != 'accepted') {
				any_not_accepted = true;
			}
		}

		var any_declined = false;
		for(var i=0; i<this.players.length; i++) {
			if (this.players[i].state == 'declined') {
				any_declined = true;
			}
		}

		if (any_declined) {
			this.cancel();
		} else if (!any_not_accepted) {
			this.start();
		}
	}

	Game.prototype.updateGameMeta = function(player, meta) {
		// is it my turn?
		if (!this.myTurn(player))
			return;

		// update meta in it.
		for (var key in meta) {
			this.meta[key] = meta[key];
			this.lastUpdated = new Date();
		}

		this._updatePlayerStates();
	}

	Game.prototype.updateProgress = function(player, meta, delta) {
		// is it my turn?
		if (!this.myTurn(player))
			return;

		if (typeof(delta) === 'undefined')
			delta = false;

		// do we have a current turn with my player as current?
		if (this.turns.length == 0 || (
			this.turns[this.turns.length-1].playerIndex != this.currentPlayerIndex ||
			this.turns[this.turns.length-1].turn != this.turn) ) {
			this.turns.push({
				turn: this.turn,
				playerIndex: this.currentPlayerIndex,
				player: player,
				meta: {}
			});
			this.lastUpdated = new Date();
		}

		if (typeof(this.turns[this.turns.length-1].meta.score) == 'undefined')
			this.turns[this.turns.length-1].meta.score = 0;

		// if we're reporting delta score, update score.
		if (meta._deltaScore) {
			this.turns[this.turns.length-1].meta.score =
				(this.turns[this.turns.length-1].meta.score || 0) + meta._deltaScore;
			delete(meta._deltaScore);
		}

		// if we sent a score, don't register it in the meta
		if (!delta) {
			this.players[this.currentPlayerIndex].score += this.turns[this.turns.length-1].meta.score || 0;
			// delete (meta.score);
		}

		if (meta.score) {
			// make sure we don't overwrite score
			delete(meta.score);
		}

		// update meta in it.
		for (var key in meta) {
			this.turns[this.turns.length-1].meta[key] = meta[key];
			this.lastUpdated = new Date();
		}

		if (!delta) {

			this.currentPlayerIndex ++;
			if (this.currentPlayerIndex >= this.players.length) {
				this.currentPlayerIndex = 0;
				this.turn ++;
				if (this.turn >= this.totalTurns) {
					// end game, calc results.
					this.state = 'over';
					// console.log('done!!');
					this._calcResults();
				}
			}
			this.lastUpdated = new Date();

		}
		this._updatePlayerStates();
	}

	Game.prototype.beginInvite = function() {
		this.currentPlayerIndex = 0;
		this.state = 'inviting';
		this.lastUpdated = new Date();
		this._updatePlayerStates();
	}

	Game.prototype.shufflePlayers = function() {
		this.lastUpdated = new Date();
		this._updatePlayerStates();
	}

	Game.prototype.start = function() {
		this.currentPlayerIndex = 0;
		this.state = 'active';
		this.lastUpdated = new Date();
		this._updatePlayerStates();
	}

	Game.prototype.cancel = function() {
		this.state = 'cancelled';
		this.lastUpdated = new Date();
		this._updatePlayerStates();
	}

	exports.Game = Game;

})(exports);
