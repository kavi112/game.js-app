(function(exports) {

	var restify = require('restify');
	var uuid = require('node-uuid');

	var config = require('./config');
	var model = require('./model');
	var AuthHelper = require('./authhelper').AuthHelper;

	exports.setup = function(server, db) {

		server.post('/user/auth', function (req, res, next) {
			console.log('user auth');
			console.log(req.query);
			console.log(req.params);
			console.log('\n\n');
			res.contentType = 'text/plain';

			AuthHelper.validateUsernameAndPassword(req.params.username, req.params.password, function(result) {
				console.log('validate username result', result);
				if (result.valid) {
					res.send('' + result.id);
				} else {
					res.send('');
				}
				next();
			});
		});

		server.post('/user/auth/createtoken', function (req, res, next) {
			console.log('create token!');
			AuthHelper.createToken(function(result2) {
				console.log('create token result', result2);
				// connect token to user.
				res.contentType = 'text/plain';
				if (result2.valid) {
					res.send('' + result2.id);
				} else {
					res.send('');
				}
				next();
			});
			return null;
		});

		var tempmemory = {};

		server.get('/user/auth/facebook', function (req, res, next) {
			console.log('user facebook auth');
			console.log('params', req.params);
			// console.log(req.params);
			// console.log('\n\n');
			if (req.query.code) {
				var code = req.query.code;
				console.log('query', req.query);
				// query behind the scenes....
				var client = restify.createStringClient({
					url: 'https://graph.facebook.com'
				})
				var path = '/oauth/access_token?client_id=' + config.facebookAppId + '&redirect_uri=' + config.facebookRedirect + '&client_secret='  + config.facebookSecret + '&code=' + encodeURIComponent(code);

				client.get(path, function(err2, req2, res2, data) {
					console.log('got data: %s', data);
					// res.contentType = 'text/html';
					// res.send('access token: '+data);
					var redir = '/user/auth/facebook/done?'+data+'&state='+req.query.state;
					res.setHeader('Location', redir);
					res.send(301, '<a href=\"'+redir+'\">'+redir+'</a>');
					return next();
				});
			} else {
				var redirect_url = req.params.redirect;
				console.log('redirect to', redirect_url);
				var stateid = uuid.v4();
				tempmemory[stateid] = { redirect: redirect_url };
				var redir = 'https://www.facebook.com/dialog/oauth?client_id=' + config.facebookAppId + '&display=popup&redirect_uri=' + config.facebookRedirect + '&state='+stateid;
				res.contentType = 'text/html';
				res.setHeader('Location', redir);
				res.send(301, '<a href=\"'+redir+'\">'+redir+'</a>');
				return next();
			}
		});

		server.get('/user/auth/facebook/done', function (req, res, next) {
			console.log('user auth');
			var access_token = req.query.access_token
			console.log('access_token', access_token);
			var memstate = tempmemory[req.query.state];
			console.log('memstate', memstate);
			var redirect_url = memstate.redirect;
			console.log('query', req.query);
			var client = restify.createStringClient({
				url: 'https://graph.facebook.com'
			})
			var path = '/me?access_token=' + access_token;
			client.get(path, function(err2, req2, res2, data) {
				console.log('got data: %s', data);
				data = JSON.parse(data);

				// TODO: get user with this id.

				var avatar = 'http://graph.facebook.com/' + data.id + '/picture?width=200&height=200';

				AuthHelper.getOrCreateFacebookToken(
					data.id,
					data.name,
					avatar,
					access_token,
					function(result2) {
						console.log('get or create token result', result2);
						// connect token to user.
						if (redirect_url != '') {
							var redir = redirect_url;
							if (redir.indexOf('?') == -1)
								redir += '?';
							else
								redir += '&';
							redir += 'token=' + result2.id;
							res.contentType = 'text/html';
							res.setHeader('Location', redir);
							res.send(301, '<a href=\"'+redir+'\">'+redir+'</a>');
						} else {
							if (result2.valid) {
								res.send('' + result2.id);
							} else {
								res.send('');
							}
						}
						next();
					}
				);
			});
		});

		server.get('/invite', function(req, res, next) {
			/*
			Send game request via facebook:
			https://developers.facebook.com/docs/concepts/requests/
			FB.init({ appId: '**appId**', status: true, cookie: true, xfbml : true });
			function sendRequest(to) {
			FB.ui({method: 'apprequests', to: to, message: 'You should learn more about this awesome site.', data: 'tracking information for the user'});
			return false;
			}
			*/
			next();
			return null;
		});

		server.get('/friend', function(req, res, next) {
			var friend_id = req.params.id.split(':');
			console.log('look up friend by id', friend_id);
			console.log(req.params.access_token);
			if (friend_id[0] == 'facebook') {
				// look up facebook friend.
				AuthHelper.forRequest(req, function(token) {
					console.log('token for request', token);
					// do we have a registered user with this facebook id already?
					var path = '/'+friend_id[1]+'?access_token=' + token.metadata.facebookToken;
					// var avatar = 'http://graph.facebook.com/' + friend_id[1] + '/picture?width=200&height=200';
					var client = restify.createJsonClient({
						url: 'https://graph.facebook.com'
					})
					client.get(path, function(err2, req2, res2, data) {
						console.log('got friend: %s', data);
						var avatar = 'http://graph.facebook.com/' + friend_id[1] + '/picture?width=200&height=200';
						AuthHelper.getOrCreateFacebookToken(friend_id[1], data.name, avatar, '', function(token2) {
							var result = {
								id: token2.userId
							};
							res.contentType = 'application/json';
							res.send(result);
							next();
						});
					});
				});
			}
			return null;
		});

		server.get('/friends', function(req, res, next) {
			console.log('get my friends!');
			var query = req.params.q;
			console.log(req.params.access_token);
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				var path = '/'+token.metadata.facebookId+'/friends?access_token=' + token.metadata.facebookToken;
				var client = restify.createJsonClient({
					url: 'https://graph.facebook.com'
				})
				client.get(path, function(err2, req2, res2, data) {
					console.log('got friends: %s', data);
					// data = JSON.parse(data);
					var result = [];
					data.data.forEach(function(friend) {
						if (friend.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
							var avatar = 'http://graph.facebook.com/' + friend.id + '/picture?width=200&height=200';
							result.push({
								type: 'facebook',
								id: 'facebook:'+friend.id,
								facebookId: friend.id,
								name: friend.name,
								avatar: avatar
							});
						}
					});
					console.log('returning', result);
					// res.setEncoding('utf8');
					res.contentType = 'application/json';//; charset=uf-8';
					// res.charset = 'UTF-8';
					res.send(result);
					// res.send('åäö hej');
					next();
				});
			});
			return null;
		});

		server.get('/me', function (req, res, next) {

			console.log('info about me!');
			console.log(req.params.access_token);
			AuthHelper.forRequest(req, function(token) {
				console.log('token for request', token);
				model.User.find({
					_id: token.userId
				}, function(err, user) {
					console.log('user for request', user);
					res.contentType = 'application/json';
					if (user)
						res.send({
							id: user[0]._id,
							name: user[0].name,
							avatar: user[0].avatar
						});
					else
						res.send({});
					next();
				});
			});
			return null;
		});

	}

})(exports);
