var assert = require("assert");
var Game = require('../server/gamelogic.js').Game;

describe('Game logic', function() {
	describe('newGame', function() {
		it('should have a nice initial state', function() {
			var game = new Game();
			assert.equal(0, game.turn);
			assert.equal(0, game.turns.length);
			assert.equal('undefined', typeof(game.results));
			assert.equal(-1, game.currentPlayerIndex);
			assert.equal('', game.currentPlayerId);
			assert.equal(3, game.totalTurns);
			assert.equal('new', game.state);
		})
	})



	describe('addPlayer', function() {
		it('should have nice default state', function() {
			var game = new Game();
			game.addPlayer('a');
			game.addPlayer('b');
			assert.equal('invited', game.players[0].state);
			assert.equal('a', game.players[0].id);
			assert.equal(0, game.players[0].turn);
			assert.equal(0, game.players[0].score);
			assert.equal('', game.players[0].token);
			assert.equal('invited', game.players[1].state);
			assert.equal('b', game.players[1].id);
			assert.equal(0, game.players[1].turn);
			assert.equal(0, game.players[1].score);
			assert.equal('', game.players[1].token);
		})
		it('should handle multiple players', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			assert.equal('p2', game.players[1].id);
			assert.equal('p3', game.players[2].id);
		})
	})



	describe('start', function() {
		it('should set current player to zero', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			assert.equal(0, game.currentPlayerIndex);
			assert.equal('p1', game.currentPlayerId);
		})
		it('should set \"active\"-state', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			assert.equal('active', game.state);
		})
		it('should set correct state for all players', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			assert.equal('p1', game.players[0].id);
			assert.equal('p2', game.players[1].id);
			assert.equal('p3', game.players[2].id);
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('pending', game.players[2].state);
		})
	})



	describe('updateProgress', function() {
		it('should update current player and set meta', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			game.updateProgress('p1', {dummy: 1234})
			assert.equal('pending', game.players[0].state);
			assert.equal('current', game.players[1].state);
			assert.equal('p2', game.currentPlayerId);
			assert.equal(1234, game.turns[0].meta.dummy);
		})
		it('should only allow current player', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('pending', game.players[2].state);
			assert.equal(0, game.turns.length);
			assert.equal(0, game.currentPlayerIndex);
			assert.equal('p1', game.currentPlayerId);
			game.updateProgress('p3', {dummy: 123})
			game.updateProgress('p2', {dummy: 123})
			assert.equal(0, game.currentPlayerIndex);
			assert.equal('p1', game.currentPlayerId);
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('pending', game.players[2].state);
			assert.equal(0, game.turns.length);
		})
		it('should select next player after turn', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			game.updateProgress('p1', {dummy: 123})
			assert.equal(1, game.currentPlayerIndex);
			assert.equal('p2', game.currentPlayerId);
			assert.equal('pending', game.players[0].state);
			assert.equal('current', game.players[1].state);
			assert.equal('pending', game.players[2].state);
			game.updateProgress('p2', {dummy: 123})
			assert.equal(2, game.currentPlayerIndex);
			assert.equal('p3', game.currentPlayerId);
			assert.equal('pending', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('current', game.players[2].state);
		})
		it('should loop around to first player', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			game.updateProgress('p1', {dummy: 123})
			game.updateProgress('p2', {dummy: 123})
			assert.equal(2, game.currentPlayerIndex);
			game.updateProgress('p3', {dummy: 123})
			assert.equal(0, game.currentPlayerIndex);
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('pending', game.players[2].state);
		})
		it('should have correct turn number during game', function() {
			var game = new Game({ totalTurns: 4 });
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			assert.equal(0, game.turn);
			game.start();
			assert.equal(0, game.turn);
			assert.equal('active', game.state);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal(1, game.turn);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal(2, game.turn);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal(3, game.turn);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal(4, game.turn);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal(4, game.turn); // same!
		})
		it('should have correct player state during game', function() {
			var game = new Game({ totalTurns: 2 });
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			assert.equal('pending', game.players[2].state);
			game.updateProgress('p1', {})
			assert.equal('done', game.players[0].state);
			assert.equal('current', game.players[1].state);
			assert.equal('pending', game.players[2].state);
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal('done', game.players[0].state);
			assert.equal('done', game.players[1].state);
			assert.equal('done', game.players[2].state);
		})
		it('should have correct game state during game', function() {
			var game = new Game({ totalTurns: 2 });
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			assert.equal('new', game.state);
			game.start();
			assert.equal('active', game.state);
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			assert.equal('over', game.state);
		})
		it('should handle incremental (delta) updates', function() {
			var game = new Game({ totalTurns: 2 });
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			assert.equal(0, game.turns.length);
			game.updateProgress('p1', {})
			assert.equal(1, game.turns.length);
			assert.deepEqual({}, game.turns[0].meta);
			game.updateProgress('p2', { x: 5 })
			assert.equal(2, game.turns.length);
			assert.deepEqual({ x: 5 }, game.turns[1].meta);
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			game.updateProgress('p1', { a: 2 }, true)
			assert.equal(3, game.turns.length);
			game.updateProgress('p1', { b: 3 }, true)
			assert.equal(3, game.turns.length);
			assert.equal('current', game.players[0].state);
			assert.equal('pending', game.players[1].state);
			game.updateProgress('p1', { c: 4 }, true)
			assert.equal(3, game.turns.length);
			game.updateProgress('p1', { d: 5 }, false)
			assert.equal(3, game.turns.length);
			assert.equal('done', game.players[0].state);
			assert.equal('current', game.players[1].state);
		})
		it('should end after game is over', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			assert.equal('active', game.state);
			game.updateProgress('p1', {dummy: 123})
			game.updateProgress('p2', {dummy: 123})
			game.updateProgress('p1', {dummy: 123})
			game.updateProgress('p2', {dummy: 123})
			game.updateProgress('p1', {dummy: 123})
			game.updateProgress('p2', {dummy: 123})
			assert.equal('over', game.state);
		})
		it('should add up scores', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			assert.equal(0, game.players[0].score);
			assert.equal(0, game.players[1].score);
			game.updateProgress('p1', {score: 10})
			assert.equal(10, game.players[0].score);
			game.updateProgress('p2', {score: 22})
			assert.equal(22, game.players[1].score);
			game.updateProgress('p1', {})
			assert.equal(10, game.players[0].score);
			game.updateProgress('p2', {score: 4})
			assert.equal(26, game.players[1].score);
			game.updateProgress('p1', {score: 32})
			game.updateProgress('p2', {score: 34})
			assert.equal(42, game.players[0].score);
			assert.equal(60, game.players[1].score);
			assert.equal('over', game.state);
		})
		it('should calculate result after game is over', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.addPlayer('p3');
			game.start();
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p3', {})
			game.updateProgress('p1', {score: 10})
			game.updateProgress('p2', {score: 30})
			game.updateProgress('p3', {score: 20})
			assert.equal('over', game.state);
			assert.equal('p2', game.result.winner);
			assert.equal(false, game.result.draw);
			assert.deepEqual({player: 'p2', score: 30}, game.result.players[0]);
			assert.deepEqual({player: 'p3', score: 20}, game.result.players[1]);
			assert.deepEqual({player: 'p1', score: 10}, game.result.players[2]);
		})
		it('can handle draw', function() {
			var game = new Game();
			game.addPlayer('p1');
			game.addPlayer('p2');
			game.start();
			game.updateProgress('p1', {})
			game.updateProgress('p2', {})
			game.updateProgress('p1', {score: 10})
			game.updateProgress('p2', {score: 10})
			game.updateProgress('p1', {score: 20})
			game.updateProgress('p2', {score: 20})
			assert.equal(30, game.players[0].score);
			assert.equal(30, game.players[1].score);
			assert.equal('over', game.state);
			assert.equal('', game.result.winner);
			assert.equal(true, game.result.draw);
			assert.deepEqual({player: 'p1', score: 30}, game.result.players[0]);
			assert.deepEqual({player: 'p2', score: 30}, game.result.players[1]);
		})
	})
})
