// www

var Q = require('q');
var fs = require('graceful-fs');
var uuid = require('uuid');
var restify = require('restify');
var cheerio = require('cheerio');
var crypto = require('crypto');

var rl_counter = 0;
var rl_concurrency = 5;
var rl_queue = [];
var rl_running = [];
var rl_map = {};
var	rl_waiting = 0;



function ratelimit(q) {

	var id = uuid.v4();
	console.log('id', id);

	var d = Q.defer();

	rl_queue.push(id);
	rl_map[id] = { job: q, result: d };

	/* q.done(function(data) {
	d.resolve(data);
	} */

	/*
	if (rl_waiting < rl_concurrency) {

		d.resolve(data);
	} else {

	}
	*/

	// return q;
	return d.promise;
}

setInterval(function() {
	// console.log('ratelimit tick');
	if (rl_running.length >= rl_concurrency) {
		// keep waiting for something to finish...
		console.log('waiting for jobs to finish...');
		return;
	}
	// start one
	if (rl_queue.length > 0) {
		var nextid = rl_queue[0];
		rl_queue.splice(0, 1);
		console.log('start queued job', nextid);
		rl_running.push(nextid);
		var data = rl_map[nextid];
		data.job.done(function(result) {
			console.log('queued job done', nextid);
			data.result.resolve(result);
			var idx = rl_running.indexOf(nextid);
			rl_running.splice(idx, 1);
		});
	}
}, 200);

function download_file(url) {
	var d = Q.defer();
	var md5sum = crypto.createHash('md5');
	md5sum.update(url);
	var cachefile = '/tmp/ripper.' + md5sum.digest('hex');
	console.log('download ' + url + ' to ' + cachefile);
	if (fs.existsSync(cachefile)) {
		var data = fs.readFileSync(cachefile, { encoding: 'UTF-8' });
		d.resolve(data);
	} else {
		var client = restify.createStringClient({
			url: url
		});
		client.get('', function(err, req, res, data) {
			console.log('got data', data);
			fs.writeFileSync(cachefile, data, { encoding: 'UTF-8' });
			d.resolve(data);
		});
	}
	return d.promise;
}

function parse_course(html) {
	var d = Q.defer();
	// console.log('parse course', html);
	var $ = cheerio.load(html);

	var ret = [];

	var coursename = $('h1.course-name').text();
	console.log('course name', coursename);

	$('div.thing').each(function(idx, el) {
		el = $(el);
		var col_a = el.find('.col_a .text').text();
		var col_b = el.find('.col_b .text').text();
		// console.log('thing html', col_a, col_b);
		ret.push([coursename, col_a, col_b]);
	});

	d.resolve('x');
	return d.promise;
}

function parse_category(html) {
	var all_links = [];
	var $ = cheerio.load(html);
	$('a.level').each(function(idx, el) {
		var course_url = 'http://www.memrise.com' + el.attribs.href;
		console.log('course link', course_url);
		// console.log('level el', el.attribs.href);
		all_links.push(ratelimit(download_file(course_url).then(parse_course)));
	});
	return Q.all(all_links);
}

function parse_categories(html) {
	// var d = Q.defer();
	// console.log('parsing data', html.substring(0,100));
	var page = JSON.parse(html);
	// console.log('category page', page);
	var all_links = [];
	var $ = cheerio.load(page['content']);
	$('a.inner').each(function(idx, el) {
		var category_url = 'http://www.memrise.com' + el.attribs.href;
		// console.log('category link', category_url);
		all_links.push(ratelimit(download_file(category_url).then(parse_category)));
	});
	console.log('all_links', all_links);
	return Q.all(all_links);
}

var all_categories = [];
for(var page=0; page<4; page++) {
	var category_list_url = 'http://www.memrise.com/ajax/browse/?cat=languages&sort=popular&s_cat=english&page=' + page;
	all_categories.push(ratelimit(download_file(category_list_url).then(parse_categories)));
}

Q.all(all_categories).done(function(data) {
	console.log('all categories done.', data);
});