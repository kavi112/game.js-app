//
// download a single course to csv
//

var Q = require('q');
var fs = require('graceful-fs');
var uuid = require('uuid');
var restify = require('restify');
var cheerio = require('cheerio');
var crypto = require('crypto');

function download_file(url) {
	var d = Q.defer();
	var md5sum = crypto.createHash('md5');
	md5sum.update(url);
	var cachefile = '/tmp/ripper.' + md5sum.digest('hex');
	console.log('download ' + url + ' to ' + cachefile);
	if (fs.existsSync(cachefile)) {
		var data = fs.readFileSync(cachefile, { encoding: 'UTF-8' });
		d.resolve(data);
	} else {
		var client = restify.createStringClient({
			url: url
		});
		client.get('', function(err, req, res, data) {
			// console.log('got data', data);
			fs.writeFileSync(cachefile, data, { encoding: 'UTF-8' });
			d.resolve(data);
		});
	}
	return d.promise;
}

function parse_course(sourcelanguage, targetlanguage, levelname, html) {
	var d = Q.defer();
	var $ = cheerio.load(html);
	var ret = [];
	var coursename = $('h1.course-name').text().trim();
	$('div.thing').each(function(idx, el) {
		el = $(el);
		var col_a = el.find('.col_a .text').text().trim();
		var col_b = el.find('.col_b .text').text().trim();
		// console.log('thing html', col_a, col_b, el);
		if (col_a == '' || col_b == '')
			return;
		ret.push({
			targetlanguage: targetlanguage,
			sourcelanguage: sourcelanguage,
			targetphrase: col_a,
			sourcephrase: col_b,
			course: coursename,
			level: levelname
		});
	});
	d.resolve(ret);
	return d.promise;
}

function parse_category(html) {
	var d = Q.defer();
	var all_links = [];
	var $ = cheerio.load(html);
	var coursename = $('h1.course-name').text().trim();
	var breadcrumbs = $('div.course-breadcrumb a');
	// console.log('breadcrumbs', breadcrumbs[breadcrumbs.length-1]);
	var languagelink = breadcrumbs[breadcrumbs.length-1].attribs.href;
	languagelink = languagelink.replace(/^\//, '');
	languagelink = languagelink.replace(/\/$/, '');
	languagelink = languagelink.split('/');
	// console.log('language link', languagelink);
	var sourcelanguage = languagelink[1];
	var targetlanguage = languagelink[2];
	console.log('course name', coursename);

	$('a.level').each(function(idx, el) {
		var course_url = 'http://www.memrise.com' + el.attribs.href;
		el = $(el);
		var levelname = el.find('.level-title').text().trim();
		// console.log('course link', course_url);
		all_links.push(download_file(course_url)
			.then(parse_course.bind(this, sourcelanguage, targetlanguage, levelname)));
	});

	Q.all(all_links).done(function(data) {

		var filename = coursename.toLowerCase() + '.tsv';
		filename = filename.replace(/[^a-z0-9]/g, '-');

		var merged = [];
		merged = merged.concat.apply(merged, data);

		console.log('writing ' + merged.length + ' rows to ' + filename);

		var file = '';
		merged.forEach(function(row) {
			file += [
				row.targetlanguage,
				row.sourcelanguage,
				row.course,
				row.level,
				row.targetphrase,
				row.sourcephrase
			].join('\t')+'\n';
		});

		fs.writeFileSync('data/'+filename, file, { encoding: 'UTF-8' });

		d.resolve(true);
	});

	return d.promise;
}

var url = process.argv[2];

download_file(url).then(parse_category).done(function(x) {
	console.log('all done.');
	process.exit(0);
});

