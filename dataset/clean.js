//
// clean up csv to usable json
//

var fs = require('fs');

var csv = fs.readFileSync('ripped.tsv', 'UTF-8');

function parseTime(str) {
	var a = str.split(':');
	return 60 * parseInt(a[0], 10) + parseInt(a[1], 10);
}

var ret = {
	// track_by_id: {},
	// start_by_id: {},
	// stop_by_id: {},
	groups: {},
	groups_by_target_language: {},
	groups_by_source_language: {},
	language_by_group: {},
	language_by_phrase: {},
	phrase_by_group: {},
	phrase_by_language: {},
	localized_phrase_by_id: {},
	original_phrase_by_id: {},
	original_phrase_by_group: {},
	localized_phrase_by_group: {},
	localized_phrase_by_phrase: {},
	localized_phrase_by_language: {}
};

var id = 0;
var groups = {};
var groupidgen = 0;

csv.split('\n').forEach(function(line) {
	var cols = line.split('\t');

	if (id < 5)
		console.log( cols );

	// var dur = parseInt(cols[6], 10);
	// if (dur) {
	// var st = parseTime(cols[5]);
	var tar_lang = cols[0] || '';
	var src_lang = cols[1] || '';

	if (src_lang == '' || src_lang == 'undefined' ||
		tar_lang == '' || tar_lang == 'undefined') {
		return;
	}

	tar_lang = tar_lang.replace(/\-.*$/g, '')
	src_lang = src_lang.replace(/\-.*$/g, '')

	var group = cols[2];
	var subgroup = cols[3];
	var fullgroup = group + ' / ' + subgroup;

	var src_phrase = cols[4];
	var tar_phrase = cols[5];

	var groupkey = src_lang + '--' + tar_lang + '--' + fullgroup;
	var groupid = 0;
	if (typeof(groups[groupkey]) == 'undefined') {
		groupid = groupidgen++;
		groups[groupkey] = id;
		ret.groups[groupid] = {
			id: groupid,
			sourcelanguage: src_lang,
			targetlanguage: tar_lang,
			category: group,
			subcategory: subgroup,
			title: fullgroup
		}
	} else {
		groupid = groups[groupkey];
	}

	ret.original_phrase_by_id[id] = src_phrase;
	ret.localized_phrase_by_id[id] = tar_phrase;

	if (typeof(ret.phrase_by_group[groupid]) == 'undefined') ret.phrase_by_group[groupid] = [];
	ret.phrase_by_group[groupid].push(id);

	if (typeof(ret.original_phrase_by_group[groupid]) == 'undefined') ret.original_phrase_by_group[groupid] = [];
	ret.original_phrase_by_group[groupid].push(id);

	if (typeof(ret.localized_phrase_by_group[groupid]) == 'undefined') ret.localized_phrase_by_group[groupid] = [];
	ret.localized_phrase_by_group[groupid].push(id);

	if (typeof(ret.phrase_by_language[tar_lang]) == 'undefined') ret.phrase_by_language[tar_lang] = {};
	if (typeof(ret.phrase_by_language[src_lang]) == 'undefined') ret.phrase_by_language[src_lang] = {};
	ret.phrase_by_language[tar_lang][tar_phrase] = id;
	ret.phrase_by_language[src_lang][src_phrase] = id;

	/*
	if (typeof(ret.language_by_phrase[src_phrase]) == 'undefined') ret.language_by_phrase[src_phrase] = {};
	if (typeof(ret.phrase_by_group[group]) == 'undefined') ret.phrase_by_group[group] = [];
	if (typeof(ret.localized_phrase_by_phrase[phrase]) == 'undefined') ret.localized_phrase_by_phrase[phrase] = {};
	if (typeof(ret.localized_phrase_by_phrase[loc_phrase]) == 'undefined') ret.localized_phrase_by_phrase[loc_phrase] = {};
	*/

	if (typeof(ret.localized_phrase_by_language[tar_lang]) == 'undefined') ret.localized_phrase_by_language[tar_lang] = {};
	if (typeof(ret.localized_phrase_by_language[src_lang]) == 'undefined') ret.localized_phrase_by_language[src_lang] = {};
	if (typeof(ret.localized_phrase_by_language[tar_lang][src_lang]) == 'undefined') ret.localized_phrase_by_language[tar_lang][src_lang] = {};
	if (typeof(ret.localized_phrase_by_language[src_lang][tar_lang]) == 'undefined') ret.localized_phrase_by_language[src_lang][tar_lang] = {};
	ret.localized_phrase_by_language[tar_lang][src_lang][id] = tar_phrase;
	ret.localized_phrase_by_language[src_lang][tar_lang][id] = src_phrase;

	if (typeof(ret.language_by_group[groupid]) == 'undefined') ret.language_by_group[groupid] = [];
	if (ret.language_by_group[groupid].indexOf(tar_lang) == -1) ret.language_by_group[groupid].push(tar_lang);

	/*
	ret.language_by_phrase[phrase][tar_lang] = id;
	if (ret.phrase_by_group[group].indexOf(phrase) == -1) ret.phrase_by_group[group].push(phrase);
	ret.phrase_by_language[lang][phrase] = id
	if (ret.groups.indexOf(group) == -1) ret.groups.push(group);
	*/

	if (typeof(ret.groups_by_source_language[src_lang]) == 'undefined') ret.groups_by_source_language[src_lang] = [];
	if (ret.groups_by_source_language[src_lang].indexOf(group) == -1) ret.groups_by_source_language[src_lang].push(group);

	if (typeof(ret.groups_by_target_language[tar_lang]) == 'undefined') ret.groups_by_target_language[tar_lang] = [];
	if (ret.groups_by_target_language[tar_lang].indexOf(group) == -1) ret.groups_by_target_language[tar_lang].push(group);

	/*
	ret.localized_phrase_by_phrase[tar_phrase][tar_lang] = loc_phrase;
	ret.localized_phrase_by_phrase[loc_phrase][src_lang] = phrase;
	*/

	id ++;

	// }
});

fs.writeFileSync('ripped.json', JSON.stringify(ret, null, 2), 'UTF-8');
