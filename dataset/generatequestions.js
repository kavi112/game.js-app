var questionlogic = require('../server/questionlogic.js');
console.log(questionlogic.Questions);

var q = new questionlogic.Questions();
q.loadFile('ripped.json');

console.log('Supported languages', q.getLanguages());
// console.log('All question groups', q.getAllGroups());
q.getLanguages().forEach(function(lang) {
	var grps = q.getGroupsForLanguage(lang);
	console.log('Question groups for ' + lang);
	for(var i=0; i<grps.length; i++) {
		var grp = grps[i];
		console.log('\t#'+grp.id+': '+grp.title + ' (from ' + grp.sourcelanguage+')');
	}
});

q.dumpQuestions('French (in english):', q.generateQuestions({
	languages: ['french'],
	applanguage: 'english',
	total: 8,
	groups: [ 70, 71 ]
}));

q.dumpQuestions('German (in english):', q.generateQuestions({
	languages: ['german'],
	applanguage: 'english',
	total: 8,
	groups: [ 1616 ] // German Verbs / 1 - 15
}));

q.dumpQuestions('Mandarin (in english):', q.generateQuestions({
	languages: ['mandarin'],
	applanguage: 'english',
	total: 8,
}));

q.dumpQuestions('Spanish (in english):', q.generateQuestions({
	languages: ['spanish'],
	applanguage: 'english',
	total: 8,
	groups: [ 178, 179, 180, 181, 182 ]
}));

/*
q.dumpQuestions('Learn spanish in french (any group):', q.generateQuestions({
	languages: ['spanish'],
	applanguage: 'french',
	total: 8,
	// group: 'Greetings'
}));
*/