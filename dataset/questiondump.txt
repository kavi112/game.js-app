French greetings (in english):
---------------------------------------------------------------

  How would you say "How are you?" in French?
   * Comment allez-vous? #57 (correct)
   * Salut #49 
   * S'il vous plaît #53 
   * Salutations #47 

  How would you say "Welcome" in French?
   * Bienvenue #59 (correct)
   * Excusez-moi #55 
   * Merci beaucoup #51 
   * Bonjour #61 

English personal information (in spanish):
---------------------------------------------------------------

  ¿Cuál es la palabra "Por favor" por Inglés?
   * Hello #48 
   * Welcome #58 
   * Please #52 (correct)
   * Excuse me #54 

  ¿Cuál es la palabra "Adiós" por Inglés?
   * Good Bye #64 (correct)
   * Greetings #46 
   * Good Morning #60 
   * Good Evening #62 

Spanish and french greetings (in english):
---------------------------------------------------------------

  What's the French translation for "Thank you very much"?
   * Bonsoir #63 
   * Merci beaucoup #51 (correct)
   * A tout à l'heure #67 
   * Bienvenue #59 

  What's the Spanish translation for "Excuse me"?
   * Adiós #19 
   * Por favor #7 
   * Disculpe #9 (correct)
   * ¡Hasta luego #21 

  What's the Spanish translation for "Welcome"?
   * Buenas Noches #17 
   * bienvenida #13 (correct)
   * ¿Cómo estás? #11 
   * ¡Buenos Días #15 

  What's the French translation for "Please"?
   * Salutations #47 
   * Excusez-moi #55 
   * S'il vous plaît #53 (correct)
   * Bonjour #61 

Spanish greetings (in french):
---------------------------------------------------------------

  Quel est le mot de "Bonsoir" pour espagnol?
   * Disculpe #9 
   * ¿Cómo estás? #11 
   * Buenas Noches #17 (correct)
   * ¡Hola #3 

  Quel est le mot de "Merci beaucoup" pour espagnol?
   * Muchas gracias #5 (correct)
   * Adiós #19 
   * ¡Hasta luego #21 
   * ¡Buenos Días #15 

