var sp = getSpotifyApi();
var models = sp.require('$api/models');
var auth = sp.require('$api/auth');

var phrases_dataset = {
	'greetings_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:18, end:19 },
	'greetings_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:21, end:23 },

	'hello_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:30, end:31 },
	'hello_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:33, end:34 },

	'thankyou_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:38, end:40 },
	'thankyou_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:40, end:43 },

	'please_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:48, end:49 },
	'please_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:51, end:53 },

	'excuseme_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:54, end:55 },
	'excuseme_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:60, end:62 },

	'howareyou_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:65, end:66 },
	'howareyou_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:69, end:70 },

	'welcome_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:74, end:75 },
	'welcome_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:77, end:78 },

	'goodmorning_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:78, end:79 },
	'goodmorning_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:83, end:85 },

	'goodevening_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:88, end:90 },
	'goodevening_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:96, end:97 },

	'goodbye_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:98, end:99 },
	'goodbye_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:104, end:105 },

	'seeyoulater_en': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:106, end:107 },
	'seeyoulater_es': { uri: 'spotify:track:01UpYESbVXq1ROfERPHu9v', start:110, end:112 },




	'dummy_success': { uri: 'spotify:track:7iIVWFp3iZ2Du5BH0F92Kq', start: 2.5, end: 4.0 },
	// 'dummy_success': { uri: 'spotify:track:5lgsS9LSO0ebSeg37rjJ1O', start: 0, end: 2 },
	'dummy_fail': { uri: 'spotify:track:7eb2ajpjLiCe6dj6Ijb60F', start: 0, end: 3 }
	// 'dummy_fail': { uri: 'spotify:track:0WrROc7HfXNh1wVvEwiMf3', start: 0, end: 2 }
};

// http://open.spotify.com/track/0VUwwHg5VvRZPAfF8MfkXQ

var questions_dataset = {
	'hello': {
		question_text: '\"Hello\" in spanish?',
		question_audio: 'hello_en',
		alternatives: [
			{ title: 'X', audio: 'hello_es', correct: true },
			{ title: 'X', audio: 'excuseme_es', correct: false },
			{ title: 'X', audio: 'howareyou_es', correct: false },
			{ title: 'X', audio: 'seeyoulater_es', correct: false },
			{ title: 'X', audio: 'thankyou_es', correct: false },
			{ title: 'X', audio: 'please_es', correct: false },
			{ title: 'X', audio: 'goodmorning_es', correct: false },
			{ title: 'X', audio: 'welcome_es', correct: false }
		]
	},
	'goodbye': {
		question_text: '\"Good bye\" in spanish?',
		question_audio: 'goodbye_en',
		alternatives: [
			{ title: 'X', audio: 'goodbye_es', correct: true },
			{ title: 'X', audio: 'excuseme_es', correct: false },
			{ title: 'X', audio: 'seeyoulater_es', correct: false },
			{ title: 'X', audio: 'thankyou_es', correct: false },
			{ title: 'X', audio: 'please_es', correct: false },
			{ title: 'X', audio: 'goodmorning_es', correct: false },
		]
	},
	'please': {
		question_text: '\"Please\" in spanish?',
		question_audio: 'please_en',
		alternatives: [
			{ title: 'X', audio: 'please_es', correct: true },
			{ title: 'X', audio: 'goodbye_es', correct: false },
			{ title: 'X', audio: 'excuseme_es', correct: false },
			{ title: 'X', audio: 'seeyoulater_es', correct: false },
			{ title: 'X', audio: 'thankyou_es', correct: false },
			{ title: 'X', audio: 'goodmorning_es', correct: false },
		]
	}
};

var available_questions = [ 'hello', 'goodbye', 'please' ];

var questionqueue = [];
var current_question = null;

var phrasequeue = [];
var playing_phrase = '';

function queuePhrase(id) {
	console.log('queue phrase: '+id);
	phrasequeue.push(id);
	console.log('queue:', phrasequeue);
	queueCheckPhrase();
}

var last_track_uri = '';

function loadTrack(uri, callback) {
	if (uri != last_track_uri) {
		last_track_uri = uri;
		console.log('loading track: '+uri);
		models.Track.fromURI(uri, function(t) {
			// silencing tricks.
			models.player.volume = 0.0;
			models.player.play(t);
			models.player.playing = false;
			models.player.volume = 0.0;
			setTimeout(function() {
				models.player.volume = 0.0;
				models.player.playing = false;
			}, 10);

			setTimeout(function() {
				models.player.volume = 0.0;
				models.player.playing = false;
			}, 20);

			setTimeout(function() {
				callback();
			}, 250);
		});
	}
	else {
		// track already loaded, just call callback.
		console.log('not loading track: '+uri);
		callback();
	}
}

function playPeriod(from, to, callback) {
	console.log('playPeriod from '+from+' to '+to);
	models.player.playing = false;
	models.player.position = from * 1000;
	models.player.volume = 1.0;
	models.player.playing = true;

	setTimeout(function() {
		// stop.
		console.log(models.player.position);
		console.log(models.player);
		models.player.playing = false;
		console.log('startPlayingPhrase stopped');
		callback();
	}, (to-from) * 1000 + 100);
}

function startPlayingPhrase(pid, callback) {
	console.log('startPlayingPhrase starting '+pid);
	playing_phrase = pid;

	var phrase = phrases_dataset[playing_phrase];
	console.log('phrase', phrase);

	// load track if different than current..

	loadTrack(phrase.uri, function() {
		playPeriod(phrase.start, phrase.end, function() {
			playing_phrase = '';
			queueCheckPhrase();
			if (callback) callback();
		});
	});
}

function checkPhrase() {
	console.log('checkPhrase');

	if (playing_phrase != '') {
		console.log('still playing '+playing_phrase+' ...');
		// still playing something, wait...
		return;
	}

	// any queued phrases?
	if (phrasequeue.length > 0) {
		console.log('queue:', JSON.stringify(phrasequeue));
		var qid = phrasequeue[0];
		phrasequeue.splice(0, 1);
		console.log('play queued phrase: '+qid);
		startPlayingPhrase(qid);
		// start playing phrase.
		return;
	}

	console.log('queue empty.');
}

var checkTimer = 0;

function queueCheckPhrase() {
	if (checkTimer != 0) clearTimeout(checkTimer);
	checkTimer = setTimeout(checkPhrase, 200);
}

function startGame() {
	console.log('start game');
	questionqueue = [];
	for (var i=0; i<available_questions.length; i++)
		questionqueue.push(available_questions[i]);
	questionqueue = shuffle(questionqueue);
	popQuestion();
}

function setQuestionText(txt) {
	document.getElementById('question').innerText = txt;
}

function setAltText(n, txt) {
	document.getElementById('alt'+(n+1)).innerText = txt;
}

function highlightAlt(n) {
	for (var i=0; i<4; i++) document.getElementById('alt'+(i+1)).className = (i == n) ? 'big highlight' : 'big';
}

function highlightQuestion(extraclass) {
	document.getElementById('question').className = extraclass;
}

function popQuestion() {
	if (questionqueue.length > 0 ) {
		var qid = questionqueue[0];
		questionqueue.splice(0, 1);
		current_question = questions_dataset[qid];
		startQuestion();
	} else {
		current_question = null;
		setQuestionText('All done! x/y.');
	}
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

var current_alternatives = [];

function startQuestion() {
	console.log('question')
	console.log(current_question);

	var alts = [];

	// take all incorrect answers
	for (var i=0; i<current_question.alternatives.length; i++) {
		var alt = current_question.alternatives[i];
		if (!alt.correct) {
			alts.push(alt);
		}
	}

	// shuffle them
	alts = shuffle(alts);

	// only keep three items
	alts = alts.splice(0, 3);

	// add the correct one
	for (var i=0; i<current_question.alternatives.length; i++) {
		var alt = current_question.alternatives[i];
		if (alt.correct) {
			alts.push(alt);
			continue;
		}
	}

	// shuffle them again
	alts = shuffle(alts);

	current_alternatives = alts;

	// dump'em
	console.log(current_alternatives, JSON.stringify(current_alternatives, 2, null));

	highlightAlt(-1);
	setQuestionText(current_question.question_text);
	setAltText(0, alts[0].title || '?');
	setAltText(1, alts[1].title || '?');
	setAltText(2, alts[2].title || '?');
	setAltText(3, alts[3].title || '?');

	if (current_question.question_audio) {
		startPlayingPhrase(current_question.question_audio, function() {
			startListingAlternatives();
		})
	} else {
		startListingAlternatives();
	}
}

function startListingAlternatives() {
	console.log('start listening alternatives...');
	highlightAlt(0);
	startPlayingPhrase(current_alternatives[0].audio, function() {
		highlightAlt(1);
		startPlayingPhrase(current_alternatives[1].audio, function() {
			highlightAlt(2);
			startPlayingPhrase(current_alternatives[2].audio, function() {
				highlightAlt(3);
				startPlayingPhrase(current_alternatives[3].audio, function() {
					highlightAlt(-1);
					console.log('game is ready and waiting for answer');
				});
			});
		});
	});
}

function startFeedback(correct, callback) {
	highlightQuestion(correct ? 'correct' : 'wrong');
	startPlayingPhrase(correct ? 'dummy_success' : 'dummy_fail', function() {
		setTimeout(function() {
			highlightQuestion('');
			callback();
		}, 1500);
	});
}

function sendAnswer(alt) {
	console.log('answer #'+alt);

	if (current_question && current_alternatives) {
		highlightAlt(alt);
		if (!current_alternatives[alt].correct) {
			// highlight correct alt?
			// highlightAlt(-1);
		}
		startFeedback(current_alternatives[alt].correct, function() {
			highlightAlt(-1);
			popQuestion();
		});
	} else {
		if (alt == 0) queuePhrase('hello_en');
		if (alt == 1) queuePhrase('hello_es');
		if (alt == 2) queuePhrase('goodbye_en');
		if (alt == 3) queuePhrase('goodbye_es');
	}
}

window.onload = function() {

	document.getElementById('play').addEventListener('click', function() { startGame(); });

	document.getElementById('alt1').addEventListener('click', function() { sendAnswer(0); });
	document.getElementById('alt2').addEventListener('click', function() { sendAnswer(1); });
	document.getElementById('alt3').addEventListener('click', function() { sendAnswer(2); });
	document.getElementById('alt4').addEventListener('click', function() { sendAnswer(3); });

	document.getElementById('login').addEventListener('click', function() {

		auth.showAuthenticationDialog(
			'http://lingoquiz.possan.se:8080/user/auth/facebook',
			'http://lingoquiz.possan.se:8080/user/auth/facebook/done',
			{

				onSuccess : function(response) {
					// Response will be something like 'sp://my_app_name?token=xxxxxxx'
					console.log("Success! Here's the response URL: " + response);
				},

				onFailure : function(error) {
					console.log("Authentication failed with error: " + error);
				},

				onComplete : function() { }
			}
		);

	});

	queueCheckPhrase();
}

