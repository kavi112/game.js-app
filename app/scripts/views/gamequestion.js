/*

4. spelintro
	* visa info om ämne
	* spela en introduktionsstream, som kan hoppas över, ej supertydligt hur

5. spelloop
	* tidsgräns per fråga 30 sekunder
	* möjlighet att spela upp ljuden igen
	* efter alla frågor är klara, gå till scoreboard eller practice-scoreboard

*/

var GameQuestionView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		this.options = options;
		console.log('gameloop init.', this, arguments);
		this.node = document.createElement('div');
		console.log('options', this.options);
		this.game = new window.Game({ id: this.options.game });
		console.log('game', this.game);
		this.loadstatus();
		// this.render();
	},

	loadstatus: function() {
		var self = this;
		this.node.innerHTML = 'Loading..';
		this.game.fetch({
			success: function() {
				console.log('fetched data', self.game);
				self.render();
			}
		});
	},

	render: function() {
		var self = this;
		console.log('gameloop render.');

		var meta = self.game.get('meta');

		var qidx = parseInt(this.options.index || '0', 10);

		var first = {};
		if (meta.players[meta.currentPlayerIndex].questionQueue &&
			meta.players[meta.currentPlayerIndex].questionQueue.length > 0 &&
			qidx < meta.players[meta.currentPlayerIndex].questionQueue.length)
			first = meta.players[meta.currentPlayerIndex].questionQueue[qidx];

		renderTemplate(
			this.node,
			'templates/gameloop.html',
			{
				id: this.options.game,
				index: qidx,
				gamejson: JSON.stringify(meta, false, 2),
				turn: meta.turn,
				waitingfor: meta.currentPlayerId,
				question: first.question || '?',
				questionjson: JSON.stringify(first, false, 2)
			},
			function() {
				console.log('template rendered.');

				var myturn = (meta.currentPlayerId == window.state.get('userId'));
				var mystate = '';
				for(var i=0; i<meta.players.length; i++) {
					if (meta.players[i].id == window.state.get('userId')) {
						mystate = meta.players[i].state;
					}
				}
				console.log('myturn', myturn, meta.currentPlayerId, window.state.get('userId'));
				if (meta.state == 'over' || meta.state == 'cancelled' || meta.state == 'inviting') {
					setTimeout(function() {
						window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
					}, 500);
				} else if (meta.state == 'active') {
					if (!myturn) {
						setTimeout(function() {
							window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
						}, 500);
					} else {
						if (self.options.index) {
							if (qidx >= meta.players[meta.currentPlayerIndex].questionQueue.length) {
								setTimeout(function() {
									window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
								}, 500);
							} else {
								$('div#answer_block', self.node).show();
							}
						} else {
							if (meta.turn == 0) {
								if (meta.meta && meta.meta.questions && meta.meta.questions.length > 0) {
									$('div#intro_block', self.node).show();
								} else {
									$('div#group_block', self.node).show();
								}
							} else {
								$('div#answer_block', self.node).show();
							}
						}
					}
				}

				function postAnswer(but, el) {
					console.log('postAnswer', but, el);
					var idx = $(this).attr('index');//but.currentTarget.getAttribute('index');
					console.log('idx', idx);
					if (idx) {
						idx = parseInt(idx, 10);
						var alt = first.alts[ idx ] || {};
						console.log('alt', alt);

						// self.game.setCurrentGroup(grp, self.loadstatus.bind(self));

						var req = {};
						req['question_'+qidx] = first.question || '?';
						req['answer_'+qidx] = alt.localized || '?';
						req['correct_'+qidx] = alt.correct || false;
						req['_deltaScore'] = alt.correct ? 1 : 0;

						if (qidx >= meta.players[meta.currentPlayerIndex].questionQueue.length-1) {

							self.game.postAnswer(req, function() {
								window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
							});

						} else {

							req._delta = true;
							self.game.postAnswer(req, function() {
								window.appRouter.navigate('game/' + self.options.game + '/q/' + (qidx+1), {trigger: true});
							});
						}
					}
				}

				for (var idx=0; idx<5; idx++) {
					grp = $('button#answer' + (idx + 1));
					grp.attr('index', idx);

					if (first.alts && idx < first.alts.length) {
						grp.text(first.alts[idx].localized);
						grp.click(postAnswer);
						grp.show();
					} else {
						grp.hide();
					}
				}
			}
		);
	}

});
