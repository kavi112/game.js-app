/*

3. utmana kompis
	* välj facebookpolare -> fortsätt

*/

var FindFriendView = Backbone.View.extend({

	node: null,

	initialize: function() {
		console.log('find friend init x.');
		this.node = document.createElement('div');
		this.node2 = document.createElement('div');
		this.node3 = document.createElement('div');
		this.node.appendChild(this.node2);
		this.node.appendChild(this.node3);
		this.render();
	},

	dosearch: function(query) {
		console.log('do search', query);
		var self = this;
		window.friends.search(query, function(list) {
			console.log('got friends', list);
			self.renderhits(list);
		});
	},

	renderhits: function(list) {
		console.log('render list', list);
		renderTemplate(this.node3, 'templates/findfriend-results.html', {
			friends: list
		}, function() {
			console.log('template rendered.');
		});
	},

	render: function() {
		var self = this;
		console.log('find friend render.');
		renderTemplate(this.node2, 'templates/findfriend.html', { a: 3 }, function() {
			$('#search_button', self.node).click(function() {
				var q = $('#search_query', self.node).val().trim();
				if (q != '') self.dosearch(q);
			});
			console.log('template rendered.');
			self.renderhits([]);
			// self.dosearch('');
		});
	}

});
