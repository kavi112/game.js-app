/*

6. practice-scoreboard
	* se min poäng
	* uppmana till att utmana någon
	* om man inte är inloggad kan man logga in här och utmana någon på det här spelet
	* continue practicing

7. scoreboard
	* tillbaka-knapp för att visa alla spel
	* scoreboard uppdateras efter varje round
	* se om frågorna var rätt eller fel
	* visar att en rond är under spel, visa status först när den är klar
	* när det är min tur kan jag fortsätta spelet
	* när spelet är över, [utmana samma person igen]

*/

var ScoreboardView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		this.timer1 = null;
		this.timer2 = null;
		this.options = options;
		console.log('scoreboard view init.');
		this.node = document.createElement('div');
		// this.render();
		this.game = new window.Game({ id: this.options.game });
		console.log('game', this.game);
		this.loadstatus();
		// this.render();
	},

	loadstatus: function() {
		var self = this;
		// this.node.innerHTML = 'Loading..';
		this.game.fetch({
			success: function() {
				console.log('fetched data', self.game);
				self.render();
			}
		});
	},

	render: function() {
		var self = this;
		var meta = this.game.get('meta');
		console.log('scoreboard view render');

		var mystate = '';
		for(var i=0; i<meta.players.length; i++) {
			if (meta.players[i].id == window.state.get('userId')) {
				mystate = meta.players[i].state;
			}
		}

		var num_questions_per_turn = 3;

		var t = 0;

		var players = [];
		// var mystate = '';
		for(var i=0; i<meta.players.length; i++) {
			// if (meta.players[i].id == meta.currentPlayerId) {
			//	mystate = meta.players[i].state;
			players.push(meta.players[i]);
			// }
		}

		var turns = [];
		for (var i=0; i<meta.totalTurns; i++) {
			var turn = {
				index: (i + 1),
				players: []
			};
			for(var j=0; j<meta.players.length; j++) {
				var p = {};
				p.score = 0;
				var turndata = {};
				if (meta.turns[t])
					turndata = meta.turns[t].meta;
				p.answers = [];
				for (var k=0; k<num_questions_per_turn; k++) {
					var ans = {
						index: k,
						answered: typeof(turndata['correct_' + k]) != 'undefined',
						correct: turndata['correct_' + k] || false
					};
					p.answers.push(ans);
				}
				turn.players.push(p);
				t ++;
			}
			turns.push(turn);
		}

		renderTemplate(this.node, 'templates/scoreboard.html', {
			id: this.options.game,
			gamejson: JSON.stringify(meta, false, 2),
			turn: meta.turn,
			waitingfor: meta.currentPlayerId,
			players: players,
			turns: turns
		}, function() {

			var myturn = (meta.currentPlayerId == window.state.get('userId'));
			console.log('myturn', myturn, meta.currentPlayerId, window.state.get('userId'));
			if (meta.state == 'cancelled') {
				$('div#rejected_block', self.node).show();
			} else if (meta.state == 'inviting') {
				if (mystate == 'invited') {
					$('div#invite_block', self.node).show();
				} else {
					$('div#invitesent_block', self.node).show();
					self.timer1 = setTimeout(function() {
						self.loadstatus();
					}, 5000);
				}
			} else if (meta.state == 'active') {
				if (!myturn) {
					$('div#waiting_block', self.node).show();
					self.timer1 = setTimeout(function() {
						self.loadstatus();
					}, 5000)
				} else {
					self.timer1 = setTimeout(function() {
						clearTimeout(self.timer1);
						clearTimeout(self.timer2);
						window.appRouter.navigate('game/' + self.options.game, {trigger: true});
					}, 500);
				}
			} else if (meta.state == 'over') {
			}

			console.log('template rendered.');

			$('button#accept_invite', self.node).click(function() {
				console.log('Accept invite..');
				self.game.acceptInvite(function() {
					// self.loadstatus();
					window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
				});
			});

			$('button#decline_invite', self.node).click(function() {
				console.log('Decline invite..');
				self.game.declineInvite(function() {
					// self.loadstatus();
					window.appRouter.navigate('lobby', {trigger: true});
				});
			});

			$('button#lobby', self.node).click(function() {
				console.log('Exit scoreboard..');
				clearTimeout(self.timer1);
				clearTimeout(self.timer2);
				window.appRouter.navigate('lobby', {trigger: true});
			});
		});
	}

});



