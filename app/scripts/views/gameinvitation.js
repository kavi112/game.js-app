/*

4. spelintro
	* visa info om ämne
	* spela en introduktionsstream, som kan hoppas över, ej supertydligt hur

5. spelloop
	* tidsgräns per fråga 30 sekunder
	* möjlighet att spela upp ljuden igen
	* efter alla frågor är klara, gå till scoreboard eller practice-scoreboard

*/

var GameInvitationView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		this.options = options;
		console.log('gameloop init.', this, arguments);
		this.node = document.createElement('div');
		console.log('options', this.options);
		this.game = new window.Game({ id: this.options.game });
		console.log('game', this.game);
		this.loadstatus();
		// this.render();
	},

	loadstatus: function() {
		var self = this;
		this.node.innerHTML = 'Loading..';
		this.game.fetch({
			success: function() {
				console.log('fetched data', self.game);
				self.render();
			}
		});
	},

	render: function() {
		var self = this;
		console.log('gameloop render.');

		var meta = self.game.get('meta');

		var qidx = parseInt(this.options.index || '0', 10);

		var first = {};
		if (meta.players[meta.currentPlayerIndex].questionQueue &&
			meta.players[meta.currentPlayerIndex].questionQueue.length > 0 &&
			qidx < meta.players[meta.currentPlayerIndex].questionQueue.length)
			first = meta.players[meta.currentPlayerIndex].questionQueue[qidx];

		renderTemplate(
			this.node,
			'templates/gameinvitation.html',
			{
				id: this.options.game,
				index: qidx,
				gamejson: JSON.stringify(meta, false, 2),
				turn: meta.turn,
				waitingfor: meta.currentPlayerId,
				question: first.question || '?',
				questionjson: JSON.stringify(first, false, 2)
			},
			function() {
				console.log('template rendered.');

				var myturn = (meta.currentPlayerId == window.state.get('userId'));
				console.log('myturn', myturn, meta.currentPlayerId, window.state.get('userId'));
				if (meta.state == 'cancelled') {
					$('div#rejected_block', self.node).show();
				} else if (meta.state == 'inviting') {
					if (mystate == 'invited') {
						$('div#invite_block', self.node).show();
					} else {
						$('div#invitesent_block', self.node).show();
						self.timer1 = setTimeout(function() {
							self.loadstatus();
						}, 5000);
					}
				} else {
					self.timer1 = setTimeout(function() {
						window.appRouter.navigate('game/' + self.options.game, {trigger: true});
					}, 500);
				}

				$('button#accept_invite', self.node).click(function() {
					console.log('Accept invite..');
					self.game.acceptInvite(function() {
						// self.loadstatus();
						window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
					});
				});

				$('button#decline_invite', self.node).click(function() {
					console.log('Decline invite..');
					self.game.declineInvite(function() {
						// self.loadstatus();
						window.appRouter.navigate('lobby', {trigger: true});
					});
				});

				$('button#exit_game', self.node).click(function() {
					console.log('Exit game..');
					window.appRouter.navigate('scoreboard/' + self.options.game, {trigger: true});
				});

				$('button#hide_intro', self.node).click(function() {
					console.log('Hide intro..');
					// $('div#intro_block', self.node).hide();
					// $('div#answer_block', self.node).show();
					window.appRouter.navigate('game/' + self.options.game + '/0', {trigger: true});
				});

				$('button#lobby', self.node).click(function() {
					console.log('Exit scoreboard..');
					window.appRouter.navigate('lobby', {trigger: true});
				});
			}
		);
	}

});
