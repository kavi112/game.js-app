/*

3. välj en utmaning, när det är min tur att påbörja en omgång - välj ämne:
	* slumpa tre utmaningar att välja bland
	* (refresh för fler)
	* (köp?)
	* tillbaka

*/

var ChooseLevelView = Backbone.View.extend({

	node: null,

	initialize: function() {
		console.log('choose level init.');
		this.node = document.createElement('div');
		this.render();
	},

	render: function() {
		var self = this;
		console.log('choose level render.');
		renderTemplate(this.node, 'templates/chooselevel.html', { a: 3 }, function() {
			console.log('template rendered.');

			function startGame(group) {

				window.state.set('newGameGroup', group)

				// var mode = window.state.get('newGameMode');
				var mode = window.state.get('newGameMode');
				if (mode == 'challenge') {
					var opponents = window.state.get('newGameOpponents');
					window.games.create({ opponents: opponents.split(','), random: false, level: level }, function(game) {
						console.log('created two player game', game);
						window.appRouter.navigate('game/'+game.id, {trigger: true});
					});
				} else if (mode == 'random') {
					window.games.create({ opponents: [], random: true, level: level }, function(game) {
						console.log('created random two player game', game);
						window.appRouter.navigate('game/'+game.id, {trigger: true});
					});
				} else /* if (mode == 'practice')*/ {
					window.games.create({ opponents: [], random: false, level: level, turns: 2 }, function(game) {
						console.log('created single player game', game);
						window.appRouter.navigate('game/'+game.id, {trigger: true});
					});
				}
			}

			$('button#game1', self.node).click(function() { startGame(1); });
			$('button#game2', self.node).click(function() { startGame(2); });
			$('button#game3', self.node).click(function() { startGame(3); });

			$('button#newgame', self.node).click(function() {
				window.appRouter.navigate('newgame', {trigger: true});
			});
		});
	}

});



