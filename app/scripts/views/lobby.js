/*

1. lobby
	* pågående spel
		* din tur
		* väntar på kalle / kalle spelar just nu
		- klick på pågående spel går till scoreboard
	* skapa nytt spel
	* logga in

*/

var LobbyView = Backbone.View.extend({

	node: null,
	timer1: null,

	initialize: function() {
		console.log('lobby view init.');
		this.node = document.createElement('div');
		this.render();
		window.state.set('userLanguage', 'english');
		window.games.fetch();
		var self = this;
		window.games.on('add, remove, reset, sync', function() {
			console.log('games list changed', window.games);
			console.log('game length', window.games.length);
			var g0 = window.games.at(0);
			console.log('game 0', g0);
			/* console.log('game 0', g0.get('_id'));
			console.log('game 0', g0.get('meta')); */
			self.render();
		});
	},

	update: function() {
		window.games.fetch();
	},

	render: function() {
		var self = this;
		console.log('lobby view render.');

		var invitations = [];
		var myturn = [];
		var waiting = [];
		var oldgames = [];

		window.games.forEach(function(el) {
			var pls = el.get('players');
			var ismyturn = el.get('currentPlayerId') == window.state.get('userId');
			if (pls.length > 0) {
				var opponentName = '';
				var opponentAvatar = '';
				var opponentState = '';
				for(var i=0; i<pls.length; i++) {
					if (pls[i].id != window.state.get('userId')) {
						opponentName = pls[i].name;
						opponentAvatar = pls[i].avatar;
						opponentState = pls[i].state;
					}
				}
				switch(el.get('state'))
				{
					case 'over':
					{
						var title = 'Game over against ' + opponentName;
						// var subtitle = '#' + el.get('id') + ' state=' + el.get('state') + ' myturn=' + ismyturn;
						var subtitle = 'ID #'+el.get('id') + ' Last activity: '+ el.get('lastUpdated');
						oldgames.push({
							id: el.get('id'),
							state: el.get('state'),
							title: title,
							subtitle: subtitle,
							myTurn: myturn,
							image: opponentAvatar
						});
					}
					break;
					case 'active':
					{
						if (ismyturn) {
							var title = 'My turn against ' + opponentName;
							// var subtitle = '#' + el.get('id') + ' state=' + el.get('state') + ' myturn=' + ismyturn;
							var subtitle = 'ID #'+el.get('id') + ' Last activity: '+ el.get('lastUpdated');
							myturn.push({
								id: el.get('id'),
								state: el.get('state'),
								title: title,
								subtitle: subtitle,
								myTurn: myturn,
								image: opponentAvatar
							});
						} else {
							var title = 'Waiting for ' + opponentName;
							// var subtitle = '#' + el.get('id') + ' state=' + el.get('state') + ' myturn=' + ismyturn;
							var subtitle = 'ID #'+el.get('id') + ' Last activity: '+ el.get('lastUpdated');
							waiting.push({
								id: el.get('id'),
								state: el.get('state'),
								title: title,
								subtitle: subtitle,
								myTurn: myturn,
								image: opponentAvatar
							});
						}
					}
					break;
					case 'inviting':
					{
						// if i'm being invited:
						if (opponentState == 'invited') {
							var title = 'Waiting for answer from ' + opponentName;
							var subtitle = 'Answer pending... ID #'+el.get('id')+ ' age '+el.get('age');
							invitations.push({
								id: el.get('id'),
								state: el.get('state'),
								title: title,
								subtitle: subtitle,
								myTurn: myturn,
								image: opponentAvatar
							});
						} else {
							var title = 'Invitation from ' + opponentName;
							var subtitle = 'Answer pending... ID #'+el.get('id')+ ' age '+el.get('age');
							invitations.push({
								id: el.get('id'),
								state: el.get('state'),
								title: title,
								subtitle: subtitle,
								myTurn: myturn,
								image: opponentAvatar
							});
						}
					}
					break;
				}
			}
		});
		console.log(myturn);
		console.log(waiting);
		console.log(invitations);
		console.log(oldgames);
		renderTemplate(this.node, 'templates/lobby.html', {
			userId: window.state.get('userId'),
			invitations: invitations,
			oldgames: oldgames,
			myturn: myturn,
			waiting: waiting,
			accesstoken: window.state.get('accessToken'),
			useravatar: window.state.get('userAvatar'),
			username: window.state.get('userName')
		}, function() {
			console.log('template rendered.');

			clearTimeout(this.timer1);
			self.timer1 = setTimeout(function() {
				self.update();
			}, 5000);
		});
	},

	leave: function() {
		clearTimeout(this.timer1);
	}

});



