/*

4. spelintro
	* visa info om ämne
	* spela en introduktionsstream, som kan hoppas över, ej supertydligt hur

5. spelloop
	* tidsgräns per fråga 30 sekunder
	* möjlighet att spela upp ljuden igen
	* efter alla frågor är klara, gå till scoreboard eller practice-scoreboard

*/

var GameSetupView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		this.options = options;
		console.log('gameloop init.', this, arguments);
		this.node = document.createElement('div');
		console.log('options', this.options);
		this.game = new window.Game({ id: this.options.game });
		console.log('game', this.game);
		this.loadstatus();
		// this.render();
	},

	loadstatus: function() {
		var self = this;
		this.node.innerHTML = 'Loading..';
		this.game.fetch({
			success: function() {
				console.log('fetched data', self.game);
				self.render();
			}
		});
	},

	render: function() {
		var self = this;
		console.log('gamesetup render.', window.settings.questionSetup);

		var meta = self.game.get('meta');

		// var mylang = 'spanish';
		var mylang = window.state.get('newGameLanguage') || 'english';
		var languages = window.settings.questionSetup.languages;

		renderTemplate(
			this.node,
			'templates/gamesetup.html',
			{
				id: this.options.game,
				gamejson: JSON.stringify(meta, false, 2),
				mylanguage: mylang,
				languages: languages,
				turn: meta.turn
			},
			function() {
				console.log('template rendered.');

				function postGroup(but, el) {
					console.log('postGroup', but, el);
					// var grp = but.currentTarget.innerText;
					var grp = $(but.currentTarget).attr('groupId');
					self.game.setCurrentGroup(grp, function() {
						window.appRouter.navigate('game/' + self.options.game, {trigger: true});
					});
				}

				var grp;

				var grps = window.settings.questionSetup.groups_by_language[mylang];

				for (var idx=0; idx<6; idx++) {
					grp = $('button#group' + (idx + 1));

					if (idx < grps.length) {
						grp.text(grps[idx].title);
						grp.attr('groupId', grps[idx].id);
						grp.click(postGroup);
						grp.show();
					} else {
						grp.hide();
					}

				}
			}
		);
	}

});
