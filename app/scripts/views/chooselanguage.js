/*

3. välj en utmaning, när det är min tur att påbörja en omgång - välj ämne:
	* slumpa tre utmaningar att välja bland
	* (refresh för fler)
	* (köp?)
	* tillbaka

*/

var ChooseLanguageView = Backbone.View.extend({

	node: null,

	initialize: function() {
		console.log('choose language init.');
		this.node = document.createElement('div');
		this.render();
	},

	render: function() {
		var self = this;
		console.log('choose language render.');
		renderTemplate(this.node, 'templates/chooselanguage.html', { a: 3 }, function() {
			console.log('template rendered.');

			function startGameWithLanguage(language) {
				window.state.set('newGameLanguage', language);
				window.appRouter.navigate('choosecategory', {trigger: true});
			}

			$('button#language1', self.node).click(function() { startGameWithLanguage(1); });
			$('button#language2', self.node).click(function() { startGameWithLanguage(2); });
			$('button#language3', self.node).click(function() { startGameWithLanguage(3); });

			$('button#newgame', self.node).click(function() {
				window.appRouter.navigate('newgame', {trigger: true});
			});
		});
	}

});



