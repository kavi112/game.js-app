/*

2. skapa nytt spel
	* utmana kompis
	* spela mot random / bot
	* öva själv / practice

*/

var NewGameView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		console.log('new game init', options);
		this.node = document.createElement('div');
		window.state.set('newGameLanguage', options.language);
		this.render();
	},

	render: function() {
		var self = this;
		console.log('new game render.');

		var mylang = window.state.get('userLanguage');
		var lang = window.state.get('newGameLanguage');

		renderTemplate(this.node, 'templates/newgame.html', { mylanguage: mylang, language: lang }, function() {
			console.log('template rendered x.');

			$('button#challenge_friend', self.node).click(function() {
				console.log('Challenge a friend..');
				window.state.set('newGameOpponents', '');
				window.state.set('newGameHasOpponents', true);
				window.state.set('newGameRandom', false);
				window.state.set('newGameMode', 'challenge');

				// skapa spelet här, hoppa lobbyn
				window.appRouter.navigate('findfriend', {trigger: true});
			});

			$('button#random_play', self.node).click(function() {
				console.log('Random play..');
				window.state.set('newGameOpponents', '');
				window.state.set('newGameHasOpponents', true);
				window.state.set('newGameRandom', true);
				window.state.set('newGameMode', 'random');

				// skapa spelet här, hoppa lobbyn
				window.appRouter.navigate('creategame', {trigger: true});
			});

			$('button#practice', self.node).click(function() {
				console.log('Practice..');
				window.state.set('newGameOpponents', '');
				window.state.set('newGameHasOpponents', false);
				window.state.set('newGameRandom', false);
				window.state.set('newGameMode', 'practice');

				// skapa spelet här, hoppa lobbyn
				window.appRouter.navigate('creategame', {trigger: true});
			});

			$('button#lobby', self.node).click(function() {
				window.appRouter.navigate('lobby', {trigger: true});
			});
		});
	}

});



