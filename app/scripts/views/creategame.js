/*

3. välj en utmaning, när det är min tur att påbörja en omgång - välj ämne:
	* slumpa tre utmaningar att välja bland
	* (refresh för fler)
	* (köp?)
	* tillbaka

*/

var CreateGameView = Backbone.View.extend({

	node: null,

	initialize: function() {
		var lang = window.state.get('newGameLanguage');
		var mode = window.state.get('newGameMode');

		console.log('create game init.', lang, mode);
		this.node = document.createElement('div');

		if (mode == 'challenge') {
			var opponents = window.state.get('newGameOpponents');
			window.games.create({ opponents: opponents.split(','), random: false, language: lang }, function(game) {
				console.log('created two player game', game);
				window.appRouter.navigate('scoreboard/'+game.id, {trigger: true});
			});
		} else if (mode == 'random') {
			window.games.create({ opponents: [], random: true, language: lang }, function(game) {
				console.log('created random two player game', game);
				window.appRouter.navigate('scoreboard/'+game.id, {trigger: true});
			});
		} else /* if (mode == 'practice')*/ {
			window.games.create({ opponents: [], random: false, language: lang, turns: 2 }, function(game) {
				console.log('created single player game', game);
				window.appRouter.navigate('scoreboard/'+game.id, {trigger: true});
			});
		}
	}

});



