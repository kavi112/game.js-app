/*

3. välj en utmaning, när det är min tur att påbörja en omgång - välj ämne:
	* slumpa tre utmaningar att välja bland
	* (refresh för fler)
	* (köp?)
	* tillbaka

*/

var ChooseCategoryView = Backbone.View.extend({

	node: null,

	initialize: function() {
		console.log('choose category init.');
		this.node = document.createElement('div');
		this.render();
	},

	render: function() {
		var self = this;
		console.log('choose category render.');
		renderTemplate(this.node, 'templates/choosecategory.html', { a: 3 }, function() {
			console.log('template rendered.');

			function startGameWithCategory(category) {
				window.state.set('newGameCategory', category);
				// window.appRouter.navigate('game/'+game.id, {trigger: true});
				console.log('category', category);

				var opponents = window.state.get('newGameOpponents');
				console.log('opponents', opponents);

				var level = window.state.get('newGameLevel');
				console.log('level', level);

				var lang = window.state.get('newGameLanguage');
				console.log('lang', lang);

				var rand = window.state.get('newGameRandom');
				console.log('rand', rand);

				window.games.create({
					opponents: (opponents || '').split(','),
					language: lang,
					category: category,
					random: rand,
					level: level
				}, function(game) {
					console.log('created two player game', game);
					window.appRouter.navigate('game/'+game.id, {trigger: true});
				});
			}

			$('button#category1', self.node).click(function() { startGameWithCategory(1); });
			$('button#category2', self.node).click(function() { startGameWithCategory(2); });
			$('button#category3', self.node).click(function() { startGameWithCategory(3); });

			$('button#newgame', self.node).click(function() {
				window.appRouter.navigate('newgame', {trigger: true});
			});
		});
	}

});



