/*

4. spelintro
	* visa info om ämne
	* spela en introduktionsstream, som kan hoppas över, ej supertydligt hur

5. spelloop
	* tidsgräns per fråga 30 sekunder
	* möjlighet att spela upp ljuden igen
	* efter alla frågor är klara, gå till scoreboard eller practice-scoreboard

*/

var GameIntroView = Backbone.View.extend({

	node: null,

	initialize: function(options) {
		this.options = options;
		console.log('gameloop init.', this, arguments);
		this.node = document.createElement('div');
		console.log('options', this.options);
		this.game = new window.Game({ id: this.options.game });
		console.log('game', this.game);
		this.loadstatus();
		// this.render();
	},

	loadstatus: function() {
		var self = this;
		this.node.innerHTML = 'Loading..';
		this.game.fetch({
			success: function() {
				console.log('fetched data', self.game);
				self.render();
			}
		});
	},

	render: function() {
		var self = this;
		console.log('gameloop render.');

		var meta = self.game.get('meta');

		renderTemplate(
			this.node,
			'templates/gameintro.html',
			{
				id: this.options.game,
				gamejson: JSON.stringify(meta, false, 2),
				turn: meta.turn
			},
			function() {
				console.log('template rendered.');
				$('button#hide_intro', self.node).click(function() {
					console.log('Hide intro..');
					window.appRouter.navigate('game/' + self.options.game + '/q/0', {trigger: true});
				});
			}
		);
	}

});
