var AppRouter = Backbone.Router.extend({

  routes: {
      "lobby": "loadLobby",
      "findfriend": "findFriend",
      "creategame": "createGame",
      "newgame/:lang": "newGame",
      "chooselevel": "chooseLevel",
      "chooselanguage": "chooseLanguage",
      "choosecategory": "chooseCategory",
      "lobby": "loadLobby",
      "login": "login",
      "friend/:id": "startGameWithFriend",
      "game/:id/setup": "gameSetup",
      "game/:id/intro": "gameIntro",
      "game/:id/invitation": "gameInvitation",
      "game/:id/q/:idx": "gameQuestion",
      "game/:id": "gameRedirect",
      "scoreboard/:id": "loadScoreboard",
      "auth/token": "useToken",
      "newtoken": "newToken",
      // "open": "open",
      "*actions": "defaultRoute",
  },

  // https://apps.facebook.com/lingoquiz-app/?fb_source=notification&request_ids=413417552112995%2C204973926332739&ref=notif&app_request_type=user_to_user&notif_t=app_request

 	currentView: null,
  currentPane: 0,

 	switchView: function(view) {
    console.log('switching view to ', view);

 		if (this.currentView != null) {
      console.log('hiding view', this.currentView);
 			if (this.currentView.leave)
 				this.currentView.leave();

      if (this.currentPane == 0) {
        console.log('fading out content2');
        $('#content2').addClass('faded');
        // $('#content2').fade();
      }
      else if(this.currentPane == 1) {
        console.log('fading out content1');
        $('#content1').addClass('faded');
        // $('#content1').fade();
      }

 			this.currentView = null;
 		}

 		this.currentView = view;

    if (this.currentView != null) {
      console.log('showing new view', this.currentView);
      if (this.currentView.enter)
        this.currentView.enter();
    }

    if (this.currentPane == 0) {
     console.log('adding to content1');
      $('#content1').html('');
      $('#content1').append($(this.currentView.node));
      setTimeout(function() {
        $('#content1').removeClass('faded');
      }, 250);
      this.currentPane = 1;
    }
    else if (this.currentPane == 1) {
      console.log('adding to content2');
      $('#content2').html('');
      $('#content2').append($(this.currentView.node));
      setTimeout(function() {
        $('#content2').removeClass('faded');
      }, 250);
      this.currentPane = 0;
    }
 	}
});