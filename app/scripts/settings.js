if (location.href.indexOf('localhost') != -1) {

	window.settings = {
		apiUrl : 'http://localhost:8080',
		appUrl : 'http://localhost:8000',
		rootUrl : 'http://localhost:8080'
	}

} else {

	window.settings = {
		apiUrl : 'http://lingo-api.possan.se',
		appUrl : 'http://lingoquiz.possan.se',
		rootUrl : 'http://lingo-api.possan.se'
	}

}
