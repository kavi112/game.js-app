(function(export) {

	var SpotifyAudio = function() {}

	SpotifyAudio.prototype.queueAudio = function(trackdata, callback) {
		console.log('queue audio', trackdata);
		setTimeout(function() { callback(true); }, 500);
	}

	export.Audio = SpotifyAudio;

})(export);