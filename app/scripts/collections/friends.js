window.Friends = Backbone.Collection.extend({
	root: window.settings.rootUrl,
	model: window.User,
	search: function(query, callback) {
		var url = this.root + '/friends?q='+encodeURIComponent(query)+'&access_token=' + window.state.get('accessToken');
		console.log('query friends', url);
		$.get(url, function(response) {
			console.log('post returned', response);
			var result = JSON.parse(response);
	        callback(result);
		});
	},

	lookup: function(remoteid, callback) {
		var url = this.root + '/friend?id='+encodeURIComponent(remoteid)+'&access_token=' + window.state.get('accessToken');
		console.log('query one friend', url);
		$.get(url, function(response) {
			console.log('post returned', response);
			var result = JSON.parse(response);
	        callback(result);
		});
	}
});