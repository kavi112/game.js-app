window.Games = Backbone.Collection.extend({
	root: window.settings.rootUrl,
	model: window.Game,
	url: function() {
		return this.root + '/games?access_token=' + window.state.get('accessToken')
	},
	create: function(meta, callback) {
		var url = this.root + '/game';
		url += '?players='+encodeURIComponent(meta.opponents.join(','));
		url += '&random='+encodeURIComponent(meta.random?1:0);
		url += '&level='+encodeURIComponent(meta.level || '');
		url += '&category='+encodeURIComponent(meta.category || '');
		if (meta.turns)
			url += '&turns='+encodeURIComponent(meta.turns);
		url += '&language='+encodeURIComponent(meta.language || '');
		url += '&mylanguage='+encodeURIComponent(window.state.get('userLanguage'));
		url += '&access_token=' + window.state.get('accessToken');
		$.post(url, function(response) {
			console.log('post returned', response);
			var game = new window.Game();
			var json = JSON.parse(response);
			game.set(game.parse(json));
			game.set({meta: json.meta});
			console.log('post returned game', game);
	        callback(game);
		});
	}
});