$(function() {

  window.state = new State();
  window.games = new Games();
  window.friends = new Friends();

  window.state.fetch();

  window._templateCache = {};

  window.renderTemplate = function(target, url, data, callback) {
    var cache = window._templateCache[url];
    if (typeof(cache) !== 'undefined') {
      var html = _.template(cache, data || {});
      $(target).html(html);
      callback();
    } else {
      $.get(url, function(html) {
        window._templateCache[url] = html;
        var html2 = _.template(html, data || {});
        $(target).html(html2);
            callback();
      });
    }
  }

  var app_router = new AppRouter;

  app_router.on('route:loadLobby', function() {
      console.log('load lobby.');
      this.switchView(new LobbyView({ parameter: 'xyz' }));
  });

  app_router.on('route:findFriend', function() {
      console.log('load find friends.');
      this.switchView(new FindFriendView({ parameter: 'xyz' }));
  });

  app_router.on('route:newGame', function(lang) {
      console.log('load new game.', lang);
      this.switchView(new NewGameView({ language: lang }));
  });

  app_router.on('route:createGame', function() {
      console.log('create game.');
      this.switchView(new CreateGameView({}));
  });

  app_router.on('route:chooseLevel', function() {
      console.log('load choose level.');
      this.switchView(new ChooseLevelView({ parameter: 'xyz' }));
  });

  app_router.on('route:chooseCategory', function() {
      console.log('load choose category.');
      this.switchView(new ChooseCategoryView({ parameter: 'xyz' }));
  });

  app_router.on('route:chooseLanguage', function() {
      console.log('load choose language.');
      this.switchView(new ChooseLanguageView({ parameter: 'xyz' }));
  });

  app_router.on('route:gameRedirect', function(id) {
      console.log('load game #'+id);

      var game = new window.Game({ id: id });
      game.fetch({
        success: function() {
          console.log('fetched game', game);

          var meta = game.get('meta');
          var myturn = (meta.currentPlayerId == window.state.get('userId'));

          var mystate = '';
          for(var i=0; i<meta.players.length; i++) {
            if (meta.players[i].id == window.state.get('userId')) {
              mystate = meta.players[i].state;
            }
          }

          console.log('myturn', myturn, meta.currentPlayerId, window.state.get('userId'));

          if (meta.state == 'over' || meta.state == 'cancelled' || meta.state == 'inviting') {
            window.appRouter.navigate('scoreboard/' + game.id, {trigger: true});
          } else if (meta.state == 'active') {
            if (!myturn) {
              window.appRouter.navigate('scoreboard/' + game.id, {trigger: true});
            } else {
              // if (qidx >= meta.players[meta.currentPlayerIndex].questionQueue.length) {
              //  window.appRouter.navigate('scoreboard/' + game.id, {trigger: true});
              // } else {
                if (meta.turn == 0) {
                  if (meta.meta && meta.meta.questions && meta.meta.questions.length > 0) {
                    window.appRouter.navigate('game/' + game.id + '/intro', {trigger: true});
                  } else {
                    window.appRouter.navigate('game/' + game.id + '/setup', {trigger: true});
                  }
                } else {
                  window.appRouter.navigate('game/' + game.id + '/q/0', {trigger: true});
                }
              // }
            }
          }


        }
      });

      // this.switchView(new GameRedirectView({ game: id, index: idx }));
  });

  app_router.on('route:gameQuestion', function(id, idx) {
      console.log('load game #'+id+', index '+idx);
      this.switchView(new GameQuestionView({ game: id, index: idx }));
  });

  app_router.on('route:gameInvitation', function(id) {
      console.log('load game #'+id+' invitation view');
      this.switchView(new GameInvitationView({ game: id }));
  });

  app_router.on('route:gameIntro', function(id) {
      console.log('load game #'+id+' intro view');
      this.switchView(new GameIntroView({ game: id }));
  });

  app_router.on('route:gameSetup', function(id) {
      console.log('load game #'+id+' setup view');
      this.switchView(new GameSetupView({ game: id }));
  });

  app_router.on('route:loadScoreboard', function(id) {
      console.log('load scoreboard for game #'+id);
      this.switchView(new ScoreboardView({ game: id }));
  });

  app_router.on('route:open', function() {
      console.log('open', this);
      this.switchView(new OpenView({ game: id }));
  });

  app_router.on('route:useToken', function() {
      console.log('store access token', this);
      // this.switchView(new ScoreboardView({ }));
  });

  app_router.on('route:startGameWithFriend', function(id) {
      // kolla upp id't mot servern.
      console.log('lookup friend', id);
      window.friends.lookup(id, function(friend) {
        console.log('looked up friend', friend);
        window.state.set('newGameOpponents', friend.id);
        window.appRouter.navigate('creategame', {trigger: true});
      });
  });

  app_router.on('route:newToken', function() {
      console.log('renew access token', this);
      // this.switchView(new ScoreboardView({ }));
      localStorage.clear();
      this.navigate('lobby', {trigger: true});
  });

  app_router.on('route:login', function() {
      console.log('login');
      var redir = window.settings.appUrl + '#lobby?random=' + Math.floor(Math.random() * 1000000);
      location = window.settings.apiUrl + '/user/auth/facebook?redirect=' + encodeURIComponent(redir);
  });

  app_router.on('route:defaultRoute', function(actions) {
      console.log('load default for \"'+ actions + '\"');

      var self = this;

      // kolla om vi fått en accesstoken
      var url = actions;
      console.log('url', url);

      var re = new RegExp('token=([a-z0-9]+)');
      var m = re.exec(url);
      console.log('m', m);
      if (m) {
        var accesstoken = m[1];
        // store token
        window.state.set('accessToken', accesstoken);
        window.state.save();
        setTimeout(function() {
          self.navigate('lobby', {trigger: true});
        }, 1000);
        return;
      }

      re = new RegExp('request_ids=([a-z0-9,%]+)', 'i');
      m = re.exec(url);
      console.log('m', m);
      if (m) {
        var ids = decodeURIComponent(m[1]).split(',');
        // window.state.set('request_ids', ids);

        if (ids.length > 0) {
          var id = ids[0];

          var url = window.settings.apiUrl + '/request/' + id + '?access_token=' + window.state.get('accessToken');
          console.log('GET '+url);
          $.ajax({
            url: url,
            type: 'get',
            success: function(r) {
              console.log('in get request callback', r);






              var url2 = window.settings.apiUrl + '/request/' + id + '/delete?access_token=' + window.state.get('accessToken');
              console.log('DELETE '+url2);
              $.ajax({
                url: url2,
                type: 'post',
                success: function(r) {
                  console.log('deleted request.');

                  setTimeout(function() {
                    self.navigate('lobby', {trigger: true});
                    // this.navigate('open', {trigger: true});
                  }, 1000);

                }
              });


            }
          });


        }

        /*
        ids.forEach(function(id) {
          console.log('clear request id ' + id);
          var url = window.settings.apiUrl + '/request/' + id + '/delete?access_token=' + window.state.get('accessToken');
          console.log('DELETE '+url);
          $.ajax({
            url: url,
            type: 'post',
            success: function(r) {
              console.log('in delete callback');
            }
          });
        });
        // window.state.save();
        // alert('handle requests: '+ids);
        */

        return;
      }

      this.navigate('lobby', {trigger: true});
  });

  window.appRouter = app_router;

  var url = window.settings.rootUrl + '/questions';
  $.get(url, function(response) {
    var questions = response;// JSON.parse(response);
    console.log('got question config', questions);
    window.settings.questionSetup = questions;
    Backbone.history.start();
  });


});