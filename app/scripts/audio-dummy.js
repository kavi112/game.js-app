(function(export) {

	var DummyAudio = function() {}

	DummyAudio.prototype.queueAudio = function(trackdata, callback) {
		console.log('DummyAudio::queueAudio', trackdata);
		setTimeout(function() {
			console.log('DummyAudio::queueAudio fire callback');
			callback(true);
		}, 500);
	}

	export.Audio = DummyAudio;

})(export);