window.State = Backbone.Model.extend({
	root: window.settings.rootUrl,
	sync: function(method, model, options) {
		console.log('State sync called', method, model, options);
		if (method == 'read') {
			console.log('Load state');
			model.set('accessToken', localStorage.getItem('accessToken'), { silent: true });
			model.set('gameId', localStorage.getItem('gameId'), { silent: true });
			model.set('userId', localStorage.getItem('userId'), { silent: true });
			model.set('userName', localStorage.getItem('userName'), { silent: true });
			model.set('userAvatar', localStorage.getItem('userAvatar'), { silent: true });
			this.reloadUser();
		} else if (method == 'update' || method == 'patch' || method == 'create') {
			console.log('Save state');
			localStorage.setItem('accessToken', model.get('accessToken'));
			localStorage.setItem('gameId', model.get('gameId'));
			localStorage.setItem('userId', model.get('userId'));
			localStorage.setItem('userName', model.get('userName'));
			localStorage.setItem('userAvatar', model.get('userAvatar'));
		}
	},
	reset: function() {
		self.set('accessToken', '', {silent: true});
		self.set('gameId', '', {silent: true});
		self.set('userId', '', {silent: true});
		self.set('userName', '', {silent: true});
		self.set('userAvatar', '', {silent: true});
	},
	reloadUser: function(callback) {
		var self = this;
		$.get(
			self.root + '/me?access_token=' + this.get('accessToken'),
			function(r) {
				var u = JSON.parse(r);
				console.log('user info', u);
				self.set('userId', u.id, { silent: true });
				self.set('userName', u.name, { silent: true });
				self.set('userAvatar', u.avatar, { silent: true });
				if (callback) callback();
			}
		);
	}
});