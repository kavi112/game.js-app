window.Game = Backbone.Model.extend({
	root: window.settings.rootUrl,
	id: '',
	meta: {},
	url: function() {
		return this.root + '/game/' + this.id + '?access_token=' + window.state.get('accessToken')
	},
	postAnswer: function(meta, callback) {
		var self = this;
		var url = this.root + '/game/' + this.id + '/progress?meta=' + encodeURIComponent(JSON.stringify(meta))+'&popfirstquestion=1&access_token=' + window.state.get('accessToken')
		console.log('url', url);
		$.post(url, function(response) {
			var json = JSON.parse(response);
			console.log('post returned', response);
			self.set(self.parse(json));
			self.set({meta: json.meta});
			console.log('post returned game', self);
	        callback(self);
		});
	},
	setCurrentGroup: function(group, callback) {
		var self = this;
		var lang = window.state.get('newGameLanguage') || 'French';
		console.log('getting questions...');
		var url = this.root + '/questions/generate?group=' + group + '&languages=' + lang + '&mylanguage=' + window.state.get('userLanguage');
		console.log('url', url);
		$.get(url, function(questions) {
			// var questions = JSON.parse(response);
			console.log('got questions', questions);

			var url = self.root + '/game/' + self.id + '/questions?questions=' + encodeURIComponent(JSON.stringify(questions)) + '&access_token=' + window.state.get('accessToken')
			// console.log('url', url);
			// console.log('saving questions...');
			$.post(url, function(response2) {
				// console.log('post returned', response2);
				var json = JSON.parse(response2);
				self.set(self.parse(json));
				self.set({meta: json.meta});
				// console.log('post returned game', self);
		        callback(self);
			});
		});
	},
	acceptInvite: function(callback) {
		var self = this;
		var url = this.root + '/game/' + this.id + '/accept?access_token=' + window.state.get('accessToken') + '&mylanguage=' + window.state.get('userLanguage')
		console.log('url', url);
		$.post(url, function(response) {
			var json = JSON.parse(response);
			console.log('post returned', response);
			self.set(self.parse(json));
			self.set({meta: json.meta});
			console.log('post returned game', self);
	        callback();
		});
	},
	declineInvite: function(callback) {
		var self = this;
		var url = this.root + '/game/' + this.id + '/decline?access_token=' + window.state.get('accessToken') + '&mylanguage=' + window.state.get('userLanguage')
		console.log('url', url);
		$.post(url, function(response) {
			var json = JSON.parse(response);
			console.log('post returned', response);
			self.set(self.parse(json));
			self.set({meta: json.meta});
			console.log('post returned game', self);
	        callback();
		});
	}
});